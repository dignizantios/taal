import Foundation 
import ObjectMapper 

class TourDataModel: Mappable { 

	var id: NSNumber? 
	var name: String? 
	var description: String? 
	var imageUrl: String? 
	var pageNumber: NSNumber? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		id <- map["id"] 
		name <- map["name"] 
		description <- map["description"] 
		imageUrl <- map["imageUrl"] 
		pageNumber <- map["pageNumber"] 
	}
} 

