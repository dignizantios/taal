import Foundation 
import ObjectMapper 

class ReviewDataModel: Mappable { 

	var rating: NSNumber = 0
	var title: String = ""
	var comment: String = ""
	var providerName: String = ""

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		rating <- map["rating"] 
		title <- map["title"] 
		comment <- map["comment"] 
		providerName <- map["provider_name"] 
	}
} 

