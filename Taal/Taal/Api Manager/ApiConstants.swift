//
//  ApiConstants.swift
//  Taal
//
//  Created by Vishal on 08/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Firebase


extension UIViewController {
    
    
    func logoutApi(param : [String:Any]) {
        
        let url = userLogoutURL
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        print("PARAM: ", param)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        self.showLoader()
        
        WebServices().MakePostAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            
            self.hideLoader()
            
            if statusCode == 200 {
                removeUserData()
                userDefault.removeObject(forKey: "idToken")
                if let data = response {
                    print("Data:", data)
                    let jsonData = JSON(data)
                    
                    let firebaseAuth = Auth.auth()
                    do {
                        try firebaseAuth.signOut()
                    } catch let signOutError as NSError {
                        print ("Error signing out: %@", signOutError)
                    }
                    
//                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    let obj = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    var nav = UINavigationController()
                    nav = UINavigationController(rootViewController: obj)
                    DispatchQueue.main.async {
                        makeToast(strMessage: jsonData["message"].stringValue)
                        appDelegate.window?.rootViewController = nav
                    }
                }
                appDelegate.removeTopNotification()
            }
            else if statusCode == 406 {
                makeToast(strMessage: "Account_deactivated_or_not_approved_key".localized)
            }
            else if error != nil {
                makeToast(strMessage: error?.localized ?? "")
            }
            print("Error:-\(error?.localized ?? "")")
            print("statuscode:-\(statusCode ?? 500)")
        }
    }

    
}
