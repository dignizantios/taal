//
//  VerifyPhoneViewModel.swift
//  Taal
//
//  Created by Vishal on 11/12/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class VerifyPhoneViewModel {
    
    //MARK: Variables
    fileprivate weak var theController: VerifyPhoneVC!
    var verifyId = String()
    var signupDataDict = JSON()
    var selectController = enumForLoginRegisterOTP.login
    
    //MARK: Initializer
    init(theController: VerifyPhoneVC) {
        self.theController = theController
    }
    
}

//MARK: Api Setup
extension VerifyPhoneViewModel {
    
    func loginUserAPI(param: [String:Any], completionHandlor:@escaping()->Void) {
        
        let url = userLoginURL
        print("URL: ", url)
        print("PARAM: ", param)
        
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        
        self.theController.showLoader()
        WebServices().makePostApiWithBody(name: url, params: param, headers: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            
            if statusCode == success {
                if let result = response {
                    //                    print("Result:-", result)
                    let data = JSON(result)
                    print("Data:-", data)
                    savedUserData(user: data)
                    completionHandlor()
                }
            }
            else if statusCode == notFound {
                makeToast(strMessage: response?.value(forKey: "error") as! String)
            }
            else if statusCode == unprocessableEntity {
                makeToast(strMessage: "unprocessable_Entity_key".localized)
            }
            else {
                makeToast(strMessage: error?.localized ?? "")
            }
            
        }
    }
    
    func registerUserAPI(param : [String:Any], completionHandlor : @escaping() -> Void) {
        
        let url = userRegisterURL
        print("URL: ", url)
        print("PARAM: ", param)
        
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        
        self.theController.showLoader()
        WebServices().makePostApiWithBody(name: url, params: param, headers: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            if statusCode == success {
                if let data = response {
                    let jsonData = JSON(data)
                    print("Data: ", jsonData)
                    
                    makeToast(strMessage: jsonData["message"].stringValue)
                    completionHandlor()
                }
            }
            else if statusCode == notFound {
                makeToast(strMessage: response?.value(forKey: "error") as! String)
            }
            else if error != nil {
                makeToast(strMessage: error?.localized ?? "")
            }
            else {
                
            }
        }
    }
    
    func updateMobileNumberAPI(param : [String:Any], completionHandlor:@escaping()->Void) {
        
        let url = updateUserMobileNumber
        print("URL: ", url)
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        theController.showLoader()
        WebServices().MakePutAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            
            self.theController.hideLoader()
            if let data = response {
                
                let jsonData = JSON(data)
                print("jsonData:=",jsonData)
                if statusCode == success {
                    //                    getUserData()?.user?.email = jsonData["user"]["email"].stringValue
                    //                    getUserData()?.user?.fullname = jsonData["user"]["fullname"].stringValue
                    getUserData()?.user?.mobile = jsonData["user"]["mobile"].stringValue
                    makeToast(strMessage: jsonData["message"].stringValue)
                    completionHandlor()
                }
                else if error != nil {
                    makeToast(strMessage: error?.localized ?? "")
                }
                else {
                    makeToast(strMessage: jsonData["error"].stringValue)
                }
            }
        }
    }
}


