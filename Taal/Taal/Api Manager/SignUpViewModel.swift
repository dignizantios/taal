//
//  SignUpViewModel.swift
//  Taal
//
//  Created by Vishal on 06/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON


class SignUpViewModel {
    
    //MARK: Variables
    fileprivate weak var theController: SignUpVC!
    var countryCode = "+965"
    var signupDataDict = JSON()
    
    //MARK: Initializer
    init(theController: SignUpVC) {
        self.theController = theController
    }
    
    //MARK: check Mobile register api
    func mobileEmailIsExist(param: [String:String], complitionHandlor:@escaping()->Void) {
        
        let url = userMobileEmailExistURL
        print("URL: ", url)
        print("Param: ", param)
        var header:[String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        
        self.theController.showLoader()
        WebServices().makePostApiWithBody(name: url, params: param, headers: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            if let data = response {
                let jsonData = JSON(data)
                if jsonData["success"].intValue == 1 {
                    makeToast(strMessage: jsonData["message"].stringValue)
                    return
                }
                else {
                    complitionHandlor()
                }
            }
            print("Error:", error?.localized ?? "")
            print("StatusCode: ", statusCode ?? 500)
        }
    }
}

