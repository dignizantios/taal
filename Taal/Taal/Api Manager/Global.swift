//
//  ServiceListPostfix.swift
//  Taal
//
//  Created by Vishal on 06/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import SwiftyJSON


// Global
let userDefault = UserDefaults.standard
let appDelegate = UIApplication.shared.delegate as! AppDelegate
var currentLocation = CLLocationCoordinate2D()

var deviceToken = UIDevice.current.identifierForVendor?.uuidString
var idToken : String = userDefault.value(forKey: "idToken") as! String
var playerID = ""
var isDefaultAddress = "isDefaultAddress"

var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
let loadingView: UIView = UIView()
var fcmTokenString = String()
let preferredLanguage = NSLocale.preferredLanguages[0] // get system device language

//Notification Variables
var dictNotificationData = JSON()
var isRequestListVC = false
var isNotificationChat = false
var isStartJobNotification = false
var isCompletedJobNotification = false
var isCompletedNotification = false


//addCaseDict
var isChatScreen = false
var conversationID = ""
var dictAddCase = JSON()
var handlorAcceptedQuotation:()->Void = {}


// Validate
let deviceType = 1
let defaultAddress = "defaultAddress"
let basic_username = "admin"
let basic_password = "1234567"
let user_data_key = "user data"

let isFirstTime = "isFirstTime"
let iosVersion = "iOS-1.1"
let userAppType = "1"

// MARK: Error response
let success = 200
let notFound = 404
let unprocessableEntity = 422
let invalidApi = 400
let invalidCredential = 401
let accountNotUproved = 406


//MARK: Push response
let createCase          = "create"          //add case
let acceptedCase        = "accepted"        //quotation accepted
let completedJob        = "completed"       //job completed
let cancelledJob        = "cancelled"       //job cancelled
let updateCase          = "update"          //case update
let deleteCase          = "delete"          //case delete
let startJob            = "start job"       //job started
let arrivedAtLocation   = "arrived"         //provider arrived at location
let startTracking       = "start tracking"  //start tracking
let userConfirm         = "confirm"         //user confirmed
let tracking            = "tracking"











