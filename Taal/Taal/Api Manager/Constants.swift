//
//  Constants.swift
//  Taal
//
//  Created by Vishal on 08/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

///--- User data ---
func getUserData() -> LoginDataModel? {
    if let user = userDefault.object(forKey: user_data_key) as? Data, let json = JSON(user).dictionaryObject {
        return LoginDataModel(JSON: json)
    }
    return nil
}

func setValueUserDetail(forKey: String ,forValue: String) {
    
    guard let userDetail = UserDefaults.standard.value(forKey: user_data_key) as? Data else { return }
    var data = JSON(userDetail)
    data["user"][forKey].stringValue = forValue
    
    guard let rowdata = try? data.rawData() else {return}
    userDefault.setValue(rowdata, forKey: user_data_key)
    userDefault.synchronize()
}

func savedUserData(user: JSON) {
    guard let rawData = try? user.rawData() else { return }
    userDefault.set(rawData, forKey: user_data_key)
    userDefault.synchronize()
}

func removeUserData() {
    userDefault.removeObject(forKey: user_data_key)
    userDefault.synchronize()
}


func updateUserDicData(user: JSON) {
    guard let userDetail = UserDefaults.standard.value(forKey: user_data_key) as? Data else { return }
    var data = JSON(userDetail)
    data["user"] = user
    
    guard let rowdata = try? data.rawData() else {return}
    userDefault.setValue(rowdata, forKey: user_data_key)
    userDefault.synchronize()
}


//MARK: - Reload Extensions
extension UITableView {
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
}

extension UICollectionView {
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
}

//MARK: File Manager path
extension FileManager {
    
    class func directoryUrl() -> URL? {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths.first
    }
    
    class func allRecordedData() -> [URL]? {
        if let documentsUrl = FileManager.directoryUrl() {
            do {
                let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil)
                return directoryContents.filter{ $0.pathExtension == "m4a" }
            } catch {
                return nil
            }
        }
        return nil
    }
    
}

