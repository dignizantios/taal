//
//  AppDelegate.swift
//  Taal
//
//  Created by Vishal on 20/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import Firebase
import FirebaseAuth
import FirebaseMessaging
import UserNotifications
import SwiftyJSON

protocol locationUpdateDelegate: class {
    func getUpdateLocation(_ myLocation: CLLocationCoordinate2D)
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static let shared = UIApplication.shared.delegate as! AppDelegate
    var tabBarController = UserTabBarController()
    var locationManager = CLLocationManager()
    var locationDelegate: locationUpdateDelegate?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        GMSServices.provideAPIKey(key_google_place_picker)
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        setUpRemoteNotification(application)
//        setUpLocation()
       
        removeTopNotification()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        
    }

    func removeTopNotification() {
        if #available(iOS 10.0, *) {
           let center = UNUserNotificationCenter.current()
           center.removeAllDeliveredNotifications()
           center.removeAllPendingNotificationRequests()
        }
        else {
            UIApplication.shared.cancelAllLocalNotifications()
        }
    }
    
    func setUpLocation() {
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
//        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            // setupUserCurrentLocation()
            //  manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            //  setupUserCurrentLocation()
            //  manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            openSetting()
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
//            openSetting()
            break
        default:
            break
        }
    }
    
    //MARK:- open Setting
    func openSetting() {
        let alertController = UIAlertController (title: "Taal_key".localized, message: "Go_to_settings_key".localized, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Setting_key".localized, style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        alertController.addAction(settingsAction)
        
        window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
}



//MARK: Location
extension AppDelegate : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        currentLocation = (locationManager.location?.coordinate)!
        print("Current Location:-\(currentLocation)")
        locationDelegate?.getUpdateLocation(currentLocation)
    }
}


extension AppDelegate : UNUserNotificationCenterDelegate, MessagingDelegate {
    
    //REMOTE NOTIFICATION
    func setUpRemoteNotification(_ application: UIApplication) {
        
        //remote Notifications
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (isGranted, err) in
                if err != nil {
                    //Something bad happend
                }
                else {
                    UNUserNotificationCenter.current().delegate = self
                    Messaging.messaging().delegate = self
                    
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }
        }
        else {
            // Fallback on earlier versions
        }
        
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge,.sound,.alert], completionHandler: { (granted, error) in
                DispatchQueue.main.async {
                    application.registerForRemoteNotifications()
                }
            })
        }
        else {
            let notificationSettings = UIUserNotificationSettings(types: [.badge,.sound,.alert], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(notificationSettings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func connectToFcm() {
        // Won't connect since there is no token
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                //  self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
            }
        }
        Messaging.messaging().shouldEstablishDirectChannel = false
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("==== FCM Token:  ",fcmToken)
//        connectToFcm()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        print("Device Token = ", token)
        
        UserDefaults.standard.set(token, forKey: "device_token")
        UserDefaults.standard.synchronize()
        Messaging.messaging().apnsToken = deviceToken
        
//        let fcmToken = Messaging.messaging().fcmToken
        fcmTokenString = Messaging.messaging().fcmToken ?? ""
        print("==== FCM Token:  ",fcmTokenString)
    }

    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Barker"), object: nil)
        print("Notification Data: ", JSON(notification.request.content.userInfo))
        
        let dictData = JSON(notification.request.content.userInfo)
        
//        print("convert TO json:=\(JSON.init(parseJSON: dictPushData["custom_data"].stringValue))")
        let jsonDict = JSON.init(parseJSON: dictData["custom_data"].stringValue)
        print("JSONDICT: ", jsonDict)
        dictNotificationData = jsonDict
        if jsonDict["status_id"].stringValue == "4" || jsonDict["status_id"].stringValue == "6" {
            
            let vc = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
            
            vc.mainModelView.handlorCaseChange = {
                
                isStartJobNotification = true
                appDelegate.tabBarController = UserTabBarController()
                let navigation = UINavigationController(rootViewController: appDelegate.tabBarController)
                appDelegate.tabBarController.selectedIndex = 1
                appDelegate.window?.rootViewController = navigation
            }
            
            vc.mainModelView.handlorCompleteJob = {
                isCompletedJobNotification = true
                appDelegate.tabBarController = UserTabBarController()
                let navigation = UINavigationController(rootViewController: appDelegate.tabBarController)
                appDelegate.tabBarController.selectedIndex = 1
                appDelegate.window?.rootViewController = navigation
            }
            
            vc.mainModelView.dictPushData = jsonDict
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            self.window?.rootViewController?.present(vc, animated: false, completion: nil)
        }
        else if jsonDict["job_status"].stringValue == "send_message" {
            if isChatScreen == true {
                if conversationID == jsonDict["conversation_id"].stringValue {
                    
                }
                else {
                    completionHandler([.alert, .badge, .sound])
                }
            }
            else {
                completionHandler([.alert, .badge, .sound])
            }
        }
        else {
            completionHandler([.alert, .badge, .sound])
        }
    }
    
    // While Banner Tap and App in Background mode..
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = JSON(response.notification.request.content.userInfo)
        print("info:=\(JSON(userInfo))")
        
        
        let jsonDict = JSON.init(parseJSON: userInfo["custom_data"].stringValue)
        print("JSONDICT: ", jsonDict)
        
        dictNotificationData = JSON(jsonDict)
        if jsonDict["job_status"].stringValue == "send_message" {
            if isChatScreen == false {
                isNotificationChat = true
                appDelegate.tabBarController = UserTabBarController()
                let navigation = UINavigationController(rootViewController: appDelegate.tabBarController)
                appDelegate.tabBarController.selectedIndex = 2
                appDelegate.window?.rootViewController = navigation
            }
        }
        else if jsonDict["status_id"].stringValue == "4" || jsonDict["status_id"].stringValue == "6" {
            
            let vc = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
            
            vc.mainModelView.handlorCaseChange = {
                
                isStartJobNotification = true
                appDelegate.tabBarController = UserTabBarController()
                let navigation = UINavigationController(rootViewController: appDelegate.tabBarController)
                appDelegate.tabBarController.selectedIndex = 1
                appDelegate.window?.rootViewController = navigation
            }
            
            vc.mainModelView.handlorCompleteJob = {
                isCompletedJobNotification = true
                appDelegate.tabBarController = UserTabBarController()
                let navigation = UINavigationController(rootViewController: appDelegate.tabBarController)
                appDelegate.tabBarController.selectedIndex = 1
                appDelegate.window?.rootViewController = navigation
            }
            
            vc.mainModelView.dictPushData = jsonDict
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            self.window?.rootViewController?.present(vc, animated: false, completion: nil)
        }
        else if jsonDict["job_status"].stringValue == completedJob {
            isCompletedNotification = true
            appDelegate.tabBarController = UserTabBarController()
            let navigation = UINavigationController(rootViewController: appDelegate.tabBarController)
            appDelegate.tabBarController.selectedIndex = 0
            appDelegate.window?.rootViewController = navigation
        }
        
        completionHandler()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        print(userInfo)
    }
    
    
    // While Banner Tap and App in kill mode..
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        let userInfo = JSON(userInfo)
        print("info:=\(userInfo)")
        
        let jsonDict = JSON.init(parseJSON: userInfo["custom_data"].stringValue)
        print("JSONDICT: ", jsonDict)
        
        if userInfo["custom_data"].stringValue != "" {
            
            appDelegate.tabBarController = UserTabBarController()
            let navigation = UINavigationController(rootViewController: appDelegate.tabBarController)
            appDelegate.tabBarController.selectedIndex = 0
            appDelegate.window?.rootViewController = navigation
            
            dictNotificationData = JSON(jsonDict)
            if jsonDict["job_status"].stringValue == "send_message" {
                if isChatScreen == false {
                    isNotificationChat = true
                    appDelegate.tabBarController = UserTabBarController()
                    let navigation = UINavigationController(rootViewController: appDelegate.tabBarController)
                    appDelegate.tabBarController.selectedIndex = 2
                    appDelegate.window?.rootViewController = navigation
                }
            }
            else if jsonDict["status_id"].stringValue == "4" || jsonDict["status_id"].stringValue == "6" {
                
                let vc = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
                
                vc.mainModelView.handlorCaseChange = {
                    
                    if userDefault.value(forKey: "") as? String == "" {
                        
                    }
                    
                    isStartJobNotification = true
                    appDelegate.tabBarController = UserTabBarController()
                    let navigation = UINavigationController(rootViewController: appDelegate.tabBarController)
                    appDelegate.tabBarController.selectedIndex = 1
                    appDelegate.window?.rootViewController = navigation
                }
                
                vc.mainModelView.handlorCompleteJob = {
                    isCompletedJobNotification = true
                    appDelegate.tabBarController = UserTabBarController()
                    let navigation = UINavigationController(rootViewController: appDelegate.tabBarController)
                    appDelegate.tabBarController.selectedIndex = 1
                    appDelegate.window?.rootViewController = navigation
                }
                
                vc.mainModelView.dictPushData = jsonDict
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .coverVertical
                self.window?.rootViewController?.present(vc, animated: false, completion: nil)
            }
            else if jsonDict["job_status"].stringValue == completedJob {
                isCompletedNotification = true
                appDelegate.tabBarController = UserTabBarController()
                let navigation = UINavigationController(rootViewController: appDelegate.tabBarController)
                appDelegate.tabBarController.selectedIndex = 0
                appDelegate.window?.rootViewController = navigation
            }
        }
        
        print(userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
}

/*
Build setting -> buil search -> enable bit code = yes if not work
*/
