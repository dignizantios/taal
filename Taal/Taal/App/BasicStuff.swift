//
//  BasicStuff.swift
//  Taal
//
//  Created by Vishal on 07/12/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import  MaterialComponents
import NVActivityIndicatorView


//MARK:- SetToast
func makeToast(strMessage : String){
    let messageSnack = MDCSnackbarMessage()
    messageSnack.text = strMessage
    MDCSnackbarManager.show(messageSnack)
}

extension UIViewController : NVActivityIndicatorViewable {
 
    func showLoader() {
        //        let LoaderString:String = getCommonString(key: "Loading_key")
        let LoaderType:Int = 32
        let LoaderSize = CGSize(width: 30, height: 30)
        
        startAnimating(LoaderSize, message: "", type: NVActivityIndicatorType.ballSpinFadeLoader)
    }
    
    func hideLoader() {
    
        stopAnimating()
    }
    
}

extension UIViewController {
    //MARKL - Fonts
    func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }
}

