//
//  Utility.swift
//  Taal
//
//  Created by Anil on 04/01/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialSnackbar
import MaterialComponents.MaterialSnackbar_ColorThemer

class Utility: NSObject {
    static var shared = Utility()
    
    let manager = MDCSnackbarManager()
    let message = MDCSnackbarMessage()
   
    override init() {
        manager.snackbarMessageViewBackgroundColor = ColorOrange()
    }
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    func showAlertAtBottom(strMessage:String) {
        message.text = strMessage
        manager.show(message)
    }
    
    deinit {
        print("Dealocate \(self)")
    }
}
