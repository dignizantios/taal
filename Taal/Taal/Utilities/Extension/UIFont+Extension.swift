//
//  UIFont+Extension.swift
//  Tailory
//
//  Created by YASH on 13/07/19.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit

enum themeFonts : String {
    
    case boldItalic = "AdobeClean-BoldIt"
    case boldSemi = "AdobeClean-BoldSemiCn"
    case bold = "AdobeClean-Bold"
    case regular = "AdobeClean-Regular"
    case semiCn = "AdobeClean-SemiCn"
    case semiCnItalic = "AdobeClean-SemiCnIt"
    case italic = "AdobeClean-It"
    case lightItalic = "AdobeClean-LightIt"
    case light = "AdobeClean-Light"
}

extension UIFont {

}

func themeFont(size : Float,fontname : themeFonts) -> UIFont
{
    if UIScreen.main.bounds.width <= 320
    {
        return UIFont(name: fontname.rawValue, size: CGFloat(size) - 2.0)!
        /*
        if isEnglish {
            return UIFont(name: fontname.rawValue, size: CGFloat(size) - 2.0)!
        } else {
            return UIFont(name: themeFonts.arLight.rawValue, size: CGFloat(size) - 2.0)!
        }*/
        
    }
    else
    {
        return UIFont(name: fontname.rawValue, size: CGFloat(size))!
        /*
        if isEnglish {
            return UIFont(name: fontname.rawValue, size: CGFloat(size))!
        } else {
           return UIFont(name: themeFonts.arLight.rawValue, size: CGFloat(size))!
        }*/
 
    }
    
}
