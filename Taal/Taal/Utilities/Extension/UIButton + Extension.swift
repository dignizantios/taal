//
//  UIButton + Extension.swift
//  Tailory
//
//  Created by YASH on 13/07/19.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit


extension UIButton {
    
    func setupThemeButtonUI() {
        self.setTitleColor(UIColor.white, for: .normal)
        self.backgroundColor = UIColor.appThemeBlueColor
        self.titleLabel?.font = themeFont(size: 15, fontname: .regular)
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
    }
    
//    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
//        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
//        let mask = CAShapeLayer()
//        mask.path = path.cgPath
//        layer.mask = mask
//        
//    }
    
    func roundCorners(corners: UIRectCorner, radius: Int = 8) {
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: corners,
                                     cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
}
