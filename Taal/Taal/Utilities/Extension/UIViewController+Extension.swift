//
//  UIViewController+Extension.swift
//  Tailory
//
//  Created by om on 13/07/19.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import AVFoundation
import SwiftyJSON
import AlamofireSwiftyJSON
import Alamofire
import CoreLocation
import GoogleMaps
import HCSStarRatingView
import DropDown

extension UIViewController {
    
    func stringTodate(Formatter:String,strDate:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
        //  dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate = dateFormatter.date(from: strDate)!
        return FinalDate
    }
    
    func DateToString(Formatter:String,date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
        //  dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate:String = dateFormatter.string(from: date)
        return FinalDate
    }
    
    @IBAction func btnDismissAction(_ sender:UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    func configureReviewRatingView(ratingView : HCSStarRatingView) {
        ratingView.allowsHalfStars = false
        ratingView.maximumValue = 5
        ratingView.minimumValue = 0
        ratingView.emptyStarImage = #imageLiteral(resourceName: "ic_star_unselected_small")
        ratingView.filledStarImage = #imageLiteral(resourceName: "ic_star_selected_big")
    }
    
    func configureRatingView(ratingView : HCSStarRatingView) {
        ratingView.allowsHalfStars = false
        ratingView.maximumValue = 5
        ratingView.minimumValue = 0
        ratingView.emptyStarImage = #imageLiteral(resourceName: "ic_star_unselected_small")
        ratingView.filledStarImage = #imageLiteral(resourceName: "ic_star_selected_small")
    }
    
    func setUpNavigationBarWithTitleRightAndBack(strTitle: String, barColor: UIColor, showTitle: Bool, isLeftBackButton: Bool, isRightSkipButton: Bool) {
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.barTintColor = barColor
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        //setNavigationShadow()
        
        let headerLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        headerLabel.isUserInteractionEnabled = false
        headerLabel.text = strTitle
        headerLabel.textColor = UIColor.white
        headerLabel.numberOfLines = 2
        headerLabel.textAlignment = .center
        headerLabel.font = themeFont(size: 18, fontname: .boldSemi)
        
        if showTitle == true {
            navigationItem.titleView = headerLabel
        }
        
        navigationItem.setHidesBackButton(true, animated: false)
        
        if isLeftBackButton {
            
            let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_white")?.imageFlippedForRightToLeftLayoutDirection(), style: .plain, target: self, action: #selector(backButtonAction))
            leftButton.tintColor = UIColor.white
            navigationItem.leftBarButtonItem = leftButton
            navigationItem.hidesBackButton = true
        }
        
        if isRightSkipButton {
            let rightSkipButton = UIBarButtonItem(image: UIImage(named: "ic_header_add"), style: .plain, target: self, action: #selector(plushButtonAction))
            
            rightSkipButton.setTitleTextAttributes([NSAttributedString.Key.font : themeFont(size: 14, fontname: .regular)], for: .normal)
            
            rightSkipButton.tintColor = UIColor.white
            navigationItem.rightBarButtonItem = rightSkipButton
        }
        
    }
    
    func setupTransparontsNavigationBar() {
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        navigationController?.navigationBar.isHidden = false
        
        let headerLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        headerLabel.isUserInteractionEnabled = false
        headerLabel.text = ""
        headerLabel.textColor = UIColor.white
        headerLabel.numberOfLines = 2
        headerLabel.textAlignment = .center
        headerLabel.font = themeFont(size: 18, fontname: .boldSemi)
        
        navigationItem.titleView = headerLabel
        
        transparentNavigationBar()
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_white")?.imageFlippedForRightToLeftLayoutDirection(), style: .plain, target: self, action: #selector(backButtonAction))
        leftButton.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = leftButton
        navigationItem.hidesBackButton = true
    }
    
    func transparentNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    @objc func backButtonAction() {
        self.navigationController?.popViewController(animated: false)
        
    }
    
    @objc func plushButtonAction() {
       let addAddressVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        
        addAddressVC.mainViewModel.checkAddressTitle = .addAddress
        self.navigationController?.pushViewController(addAddressVC, animated: true)
        
    }
    
    //MARK: - DropDown Config
    func configuDropDown(dropDown: DropDown, sender: UIView) {
        
        dropDown.anchorView = sender
        dropDown.direction = .any
        dropDown.dismissMode = .onTap
        //        dropDown.topOffset = CGPoint(x: 0, y: self.view.bounds.origin.y)
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropDown.width = sender.bounds.width
        dropDown.cellHeight = 40
        dropDown.backgroundColor = UIColor.white
        dropDown.textColor = UIColor.black
        dropDown.textFont = themeFont(size: 14, fontname: .regular)
        dropDown.selectionBackgroundColor = UIColor.clear
    }
    
    //MARK: Indicator in textfield
    func showActivityIndicatory(textField: UITextField) {
        
        loadingView.frame = CGRect(x: textField.frame.width-32, y: 0, width: textField.frame.height-5, height: textField.frame.height-5)
        loadingView.backgroundColor = UIColor.white
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        loadingView.isHidden = false
        
        actInd = UIActivityIndicatorView()
        actInd.frame = CGRect(x: 0, y: 0, width: textField.frame.height, height: textField.frame.height)
        actInd.style = UIActivityIndicatorView.Style.gray
        actInd.color = UIColor.appThemeBlueColor
        actInd.center = CGPoint(x: (loadingView.frame.size.height/2)-3, y: (loadingView.frame.size.height/2)-3)
        loadingView.addSubview(actInd)
        textField.addSubview(loadingView)
        actInd.startAnimating()
    }
    
    //MARK: Hide Indicator in textfield
    func stopActivityIndicatory(textField: UITextField) {
        
        loadingView.isHidden = true
        actInd.stopAnimating()
    }
    
    func addDoneButtonOnKeyboard(textfield : UITextField) {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        doneToolbar.barTintColor = UIColor.appThemeBlueColor
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem:  UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Btn_Done_key".localized, style: UIBarButtonItem.Style.done, target: self, action: #selector(doneButtonAction))
        done.tintColor = UIColor.white
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    func showAlert(_ title: String, _ message: String, _ yesButton: String = "Done".localized ,_ noButton: String = "Cancel".localized , _ done: @escaping()->Void) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertDone = UIAlertAction(title: yesButton , style: .default) { alt in
            done()
        }
        let alertCancel = UIAlertAction(title: noButton, style: .default) { alt in
            print("Cancel Tapped")
        }
        alert.addAction(alertCancel)
        alert.addAction(alertDone)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Set TimeStamp
    /*
    func timeAgoSinceDate(date:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = NSDate()
        let earliest = now.earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now as Date
        let components = calendar.dateComponents(unitFlags, from: earliest as Date,  to: latest as Date)
        
        if (components.year! >= 2) {
            return "\(components.year!) \(getCommonString(key: "Years_ago_key"))"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 \(getCommonString(key: "Years_ago_key"))"
            } else {
                return "\(getCommonString(key: "Last_year_key"))"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 \(getCommonString(key: "Month_ago_key"))"
            } else {
                return "\(getCommonString(key: "Last_month_key"))"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) \(getCommonString(key: "Weeks_ago_key"))"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 \(getCommonString(key: "Week _ago_key"))"
            } else {
                return "\(getCommonString(key: "Last_week_key"))"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) \(getCommonString(key: "Days_ago_key"))"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 \(getCommonString(key: "Day_ago_key"))"
            } else {
                return "\(getCommonString(key: "Yesterday_key"))"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 \(getCommonString(key: "Hour_ago_key"))"
            } else {
                return "\(getCommonString(key: "An_hour_ago_key"))"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) mins ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 \(getCommonString(key: "Min_ago_key"))"
            } else {
                return "\(getCommonString(key: "A_minute_ago_key"))"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) \(getCommonString(key: "seconds_ago_key"))"
        } else {
            return "\(getCommonString(key: "Just_now_key"))"
        }
        
    }
    */
    
    //MARK: - Images with String
    
    
    func getAttributedString(imgAttachment:UIImage,strPostText:String) -> NSAttributedString {
        let fullString = NSMutableAttributedString(string: "")
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = imgAttachment
        let image1String = NSAttributedString(attachment: image1Attachment)
        fullString.append(image1String)
        fullString.append(NSAttributedString(string: strPostText))
        return fullString
    }
    
    
    func getAttributedString(imgAttachment:UIImage,strPreText:String) -> NSAttributedString {
        let fullString = NSMutableAttributedString(string: "")
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = imgAttachment
        let image1String = NSAttributedString(attachment: image1Attachment)
        fullString.append(NSAttributedString(string: strPreText))
        fullString.append(image1String)
        return fullString
    }
    
    
    //MARK: - Call Method
    
    
    func callUser(strPhoneNumber:String)
    {
        
        let phone = strPhoneNumber
        
        var phoneStr: String = "telprompt://\(phone)"
        phoneStr = phoneStr.replacingOccurrences(of: "+", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: "(", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: ")", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: "-", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: " ", with: "")
        
        let urlPhone = URL(string: phoneStr)
        if UIApplication.shared.canOpenURL(urlPhone!)
        {
            //UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(urlPhone!)
            }
            
        }
        else
        {
            // KSToastView.ks_showToast("Call facility is not available!!!", duration: ToastDuration)
        }
        
    }
    
    func gotoHomeTabbar() {
        appDelegate.tabBarController = UserTabBarController()
        let navigation = UINavigationController(rootViewController: appDelegate.tabBarController)
        appDelegate.window?.rootViewController = navigation
    }
    
    //MARK:- key board hide
    func hidekeyBoardWhenTappedAround() {
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyBoard() {
        view.endEditing(true)
    }
    
    func reverseGeoCoding(latitude : Double,longitude : Double,completion:@escaping(GMSAddress)-> ())
    {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        var currentAddress = String()
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                print("address - ",address)
                let lines = address.lines as? [String]
                currentAddress = (lines?.joined(separator: ",") ?? "")
                print("Address - ",currentAddress)
                print("country code \(address)")
                completion(address)
            }
        }
    }
    
    /*func getCountryCode(){
        CLGeocoder().reverseGeocodeLocation(location, completionHandler:
            {(placemarks, error) in
                if error {println("reverse geodcode fail: \(error.localizedDescription)")}
                let pm = placemarks as [CLPlacemark]
                if pm.count > 0 { self.showAddPinViewController(placemarks[0] as CLPlacemark) }
        })
    }*/
    
    //MARK: - Configure Dropdown
    func configureDropdown(dropdown : DropDown,sender:UIControl)
    {
        dropdown.clearSelection()
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .automatic
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        //        dropdown.topOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.size.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.cancelAction = { [unowned self] in
            print("Drop down dismissed")
        }
    }
}
