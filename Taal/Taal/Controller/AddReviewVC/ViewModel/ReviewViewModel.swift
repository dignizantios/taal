//
//  ReviewViewModel.swift
//  Taal
//
//  Created by Vishal on 28/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class ReviewViewModel {
    
    //MARK:- Variables
    fileprivate weak var theController:ReviewVC!
    
    //MARK: Initialized
    init(theController:ReviewVC) {
        self.theController = theController
    }
    var dictSelectCategoryData = CategoryDataModel(JSON: JSON().dictionaryObject!)
    
    func rateProviderAPI(param:[String:Any], completionHandlor:@escaping()->Void) {
        
        let url = rateProviderURL
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        print("URL: ", url)
        print("Header: ", header)
        print("PARAM: ", param)
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        self.theController.showLoader()
        
        WebServices().MakePostAPI(name: url, params: param, header: header) { (respons, error, statusCode) in
            if statusCode == success {
                self.theController.hideLoader()
                if let dict = respons {
                    let jsonDict = JSON(dict).dictionaryObject!
                    print("jsonDict: ", jsonDict)
                    makeToast(strMessage: jsonDict["message"] as! String)
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                completionHandlor()
            }
            else if error != nil {
                print("Error: ", error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode ?? 0)
            }
        }
    }
}
