//
//  ReviewVC_TextVCDelegate.swift
//  Taal
//
//  Created by Vishal on 28/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

extension ReviewVC : UITextViewDelegate {
    
    //MARK: TextView Delegate
    func textViewDidChange(_ textView: UITextView) {
        self.mainView.lblReviewDescription.isHidden = textView.text == "" ? false : true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}


