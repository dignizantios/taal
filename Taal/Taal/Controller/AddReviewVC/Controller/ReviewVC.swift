//
//  ReviewVC.swift
//  Taal
//
//  Created by Vishal on 28/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class ReviewVC: UIViewController {

    //MARK: Variables
    lazy var mainView: ReviewView = { [unowned self] in
        return self.view as! ReviewView
        }()
    
    lazy var mainModelView: ReviewViewModel = {
        return ReviewViewModel(theController: self)
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI(theController: self)
        
        self.setUpNavigationBarWithTitleRightAndBack(strTitle: "Write_a_review_key".localized, barColor: .appThemeBlueColor, showTitle: true, isLeftBackButton: true, isRightSkipButton: false)
    }
    
}

//MARK: Button action
extension ReviewVC {
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        let vwRate = self.mainView.vwStarRating.value
        
        if (self.mainView.txtReviewHeadline.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please_fill_required_fields_key".localized)
        }
        else if (self.mainView.txtVwReviewDescription.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please_fill_required_fields_key".localized)
        }
        else if vwRate == 0 {
            makeToast(strMessage: "Please_fill_required_fields_key".localized)
        }
        else {
            
            let param = ["provideID": self.mainModelView.dictSelectCategoryData?.providerId ?? 0,
                         "rating": vwRate,
                         "comment": self.mainView.txtVwReviewDescription.text ?? "",
                         "title": self.mainView.txtReviewHeadline.text ?? ""] as [String : Any]
            print("Param: ", param)
            self.mainModelView.rateProviderAPI(param: param) {
                print("Review sucessfully add")
                self.goBack()
            }
        }
    }
}
