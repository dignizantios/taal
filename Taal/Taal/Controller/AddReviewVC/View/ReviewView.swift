//
//  ReviewView.swift
//  Taal
//
//  Created by Vishal on 28/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import HCSStarRatingView

class ReviewView: UIView {
    
    //MARK: Outlets
    @IBOutlet var lblTapRat: UILabel!
    @IBOutlet var vwStarRating: HCSStarRatingView!
    @IBOutlet var txtReviewHeadline: UITextField!
    @IBOutlet var lblReviewDescription: UILabel!
    @IBOutlet var txtVwReviewDescription: UITextView!
    @IBOutlet var btnSubmit: CustomButton!
    
    
    //MARK: SetUp
    func setUpUI(theController: ReviewVC) {
        
        [lblTapRat].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlueColor
            lbl?.font = themeFont(size: 24, fontname: .semiCn)
        }
        
        [txtReviewHeadline].forEach { (txt) in
            txt?.font = themeFont(size: 14, fontname: .regular)
            txt?.textColor = UIColor.appThemeBlueColor
            txt?.attributedPlaceholder = NSAttributedString(string: "Review_headline_key".localized.capitalized, attributes: [NSAttributedString.Key.foregroundColor: UIColor.appThemeBlueColor])
        }
        
        [lblReviewDescription].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlueColor
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        
        [txtVwReviewDescription].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlueColor
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        lblTapRat.text = "Tap_a_rate_key".localized
        lblReviewDescription.text = "Review_description_key".localized
    }
    
}
