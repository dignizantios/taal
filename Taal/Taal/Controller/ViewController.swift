//
//  ViewController.swift
//  Taal
//
//  Created by Vishal on 20/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewController: UIViewController {

    //MARK: Outlets
    @IBOutlet var lblMain: UILabel!
    
    var model : [TourDataModel] = []
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()


        //For Uncomment to get api data
//        tourDataApi()

        /*
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
            let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "TourVC") as! TourVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
 */
    }
    
    func tourDataApi() {
        
        let url = screenBase+userAppType
        var parameters = NSDictionary() as! [String : Any]
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        
        self.showLoader()
        
        WebServices().MakeGetAPI(name: url, params: parameters, header: header) { (result, error, statusCode) in
        
            self.hideLoader()
            
            if let data = result as? NSArray {
                print("Result:-\(result)")
                
                let modelData = JSON(data)
                
                for data in modelData.arrayValue {
                    print("Data:-\(data)")
                    let value = TourDataModel(JSON: data.dictionaryObject!)
                    self.model.append(value!)
                }
                
                let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "TourVC") as! TourVC
                vc.theCurrentModel.model = self.model
                self.navigationController?.pushViewController(vc, animated: true)
            }
            print("Model:\(self.model)")
            print("Error:-\(error)")
            print("statuscode:-\(statusCode)")
        }
    }
    
}

