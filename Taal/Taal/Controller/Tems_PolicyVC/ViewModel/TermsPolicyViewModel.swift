//
//  TermsPolicyViewModel.swift
//  Taal
//
//  Created by Abhay on 30/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import WebKit

class TermsPolicyViewModel {
    
    fileprivate weak var theController : TermsPolicyVC!
    var isTerms = Bool()
    var isTitile = String()
    var webView = WKWebView()
    
    //MARK: Initializer
    init(theController: TermsPolicyVC) {
        self.theController = theController
    }
    
    func getPagesDataURl(param: [String:Any], completionHandlor:@escaping(JSON)->Void) {
        let url = termsAndPrivacyURL
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        self.theController.showLoader()
        WebServices().MakePostAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            
            if statusCode == success {
                if let dict = response {
                    let jsonDict = JSON(dict)
                    print("JsonDict: ", jsonDict)
                    completionHandlor(jsonDict)
                }
            }
            else if error != nil {
                print("Error: ", error?.localized ?? "")
            }
            else {
                print(statusCode ?? 0)
            }
        }
    }
}
