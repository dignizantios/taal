//
//  TourVC_CollectionDelegate.swift
//  Taal
//
//  Created by Vishal on 25/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

//MARK: CollectionView Delegate/Datasource

/*
extension TourVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TourCollectionCell", for: indexPath) as! TourCollectionCell
        
        let dict = self.arrayData[indexPath.row]
        
        cell.lblTitle.text = dict["title"].stringValue
        cell.lblDescription.text = dict["description"].stringValue
        cell.imgTitle.image = UIImage(named: dict["img"].stringValue)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func redirectToCell(indexPath:IndexPath){
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let width = scrollView.frame.width  - (scrollView.contentInset.left*2)
        let index = scrollView.contentOffset.x/width
        let roundedIndex = round(index)
        self.pageController.currentPage = Int(roundedIndex)
        self.currentPage = self.pageController.currentPage
//        print("currentPage:-", self.pageController.currentPage)
    }
//
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.pageController.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        pageController.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
}
*/
