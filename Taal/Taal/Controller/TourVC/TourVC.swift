//
//  TourVC.swift
//  Taal
//
//  Created by Vishal on 21/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage

class TourVC: UIViewController {

    
    //MARK: Outlets
    @IBOutlet var vwGetStarted: UIView!
    @IBOutlet var btnGetStarted: UIButton!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var vwBottom: UIView!
    @IBOutlet var btnSkip: UIButton!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnFinish: UIButton!
    @IBOutlet var pageController: UIPageControl!
    var currentPage = Int()
    
    
    //MARK: Variables
    lazy var theCurrentModel: TourModel = {
        return TourModel(theController: self)
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerXib()
        setupUI()
       
        let loginVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(loginVC, animated: false)
        
//        if userDefault.bool(forKey: isFirstTime) == false {
//            self.theCurrentModel.tourDataApi {
//                self.setupPageControl(totalPages: self.theCurrentModel.model.count)
//                self.collectionView.reloadData {
//                    if preferredLanguage == "ar" {
//                        self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: false)
//                    }
//                }
//            }
//        }
//        else {
//            let loginVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//            self.navigationController?.pushViewController(loginVC, animated: false)
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
//        UIApplication.shared.statusBarView?.backgroundColor = .white
    }
    
    override func viewDidDisappear(_ animated: Bool) {
//        UIApplication.shared.statusBarView?.backgroundColor = ColorNavigationStatusBar()
    }
    
}

//MARK: SetUP
extension TourVC {
    
    func setupUI() {
        
        btnGetStarted.setTitle("Get_started_key".localized.capitalized, for: .normal)
        btnGetStarted.setupThemeButtonUI()
        
        btnFinish.setTitle("Finish_key".localized, for: .normal)
        btnFinish.setupThemeButtonUI()
        
        btnSkip.setTitle("Skip_key".localized, for: .normal)
        btnSkip.titleLabel?.font = themeFont(size: 15, fontname: .regular)
        btnSkip.setTitleColor(UIColor.appThemeBlueColor, for: .normal)
        
        btnNext.setupThemeButtonUI()
        btnNext.setImage(UIImage(named: "ic_left_arrow_white")?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        btnNext.setTitle("", for: .normal)
        btnNext.tintColor = .white
        
        [btnFinish,btnSkip,btnNext].forEach { (btn) in
            btn?.isHidden = true
        }
        self.vwBottom.isHidden = true
        
        collectionView.register(UINib(nibName: "TourCell", bundle: nil), forCellWithReuseIdentifier: "TourCell")
        collectionView.reloadData()
        
    }
    
    func updatePageControl(index:Int){
        print("Index: ", index)
        pageController.currentPage = index
    }
    
    func setupPageControl(totalPages:Int){
        pageController.hidesForSinglePage = true
        pageController.pageIndicatorTintColor = UIColor.lightGray
        pageController.currentPageIndicatorTintColor = UIColor.appThemeBlueColor
        pageController.numberOfPages = totalPages
    }
    
    func btnClickOnGetStarted(){
        btnGetStarted.isHidden = true
        [btnSkip,btnFinish,btnNext].forEach { (btn) in
            btn?.isHidden = false
        }
        self.vwBottom.isHidden = false
        self.vwGetStarted.isHidden = true
    }
    
    func btnShowGetStartedButton() {
        self.vwBottom.isHidden = true
        self.vwGetStarted.isHidden = false
        btnGetStarted.isHidden = false
        [btnSkip,btnFinish,btnNext].forEach { (btn) in
            btn?.isHidden = true
        }
    }
    
    func btnShowFinishButton(){
        btnFinish.isHidden = false
        self.vwBottom.isHidden = false
        self.vwGetStarted.isHidden = true
        [btnSkip,btnGetStarted,btnNext].forEach { (btn) in
            btn?.isHidden = true
        }
    }
    
    func btnShowNextButton(){
        self.vwBottom.isHidden = false
        self.vwGetStarted.isHidden = true
        [btnFinish,btnGetStarted].forEach { (btn) in
            btn?.isHidden = true
        }
        [btnNext,btnSkip].forEach { (btn) in
            btn?.isHidden = false
        }
    }
    
    func registerXib() {
        
        self.collectionView.register(UINib(nibName: "TourCollectionCell", bundle: nil), forCellWithReuseIdentifier: "TourCollectionCell")
    }
    
}

//MARK: Button Action
extension TourVC {
    
    @IBAction func btnGetStartedAction(_ sender: Any) {
        if theCurrentModel.model.count < theCurrentModel.currentIndex{
            let login = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(login, animated: true)
            return
        }
        theCurrentModel.currentIndex += 1
        updatePageControl(index: theCurrentModel.currentIndex)
        btnShowNextButton()
        redirectToCell(indexPath: IndexPath(item: theCurrentModel.currentIndex, section: 0))
    }
    
    @IBAction func btnSkipAction(_ sender: Any) {
        let login = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(login, animated: true)
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
        if theCurrentModel.model.count-1 == theCurrentModel.currentIndex{
            let login = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(login, animated: true)
            return
        }
        theCurrentModel.currentIndex += 1
        redirectToCell(indexPath: IndexPath(item: theCurrentModel.currentIndex, section: 0))
        updatePageControl(index: theCurrentModel.currentIndex)
        if theCurrentModel.currentIndex == 0 {
            btnShowGetStartedButton()
        } else if (theCurrentModel.currentIndex + 1) == theCurrentModel.model.count{
            btnShowFinishButton()
        } else {
            btnShowNextButton()
        }
    }
    
    @IBAction func btnFinishAction(_ sender: Any) {
        
        let login = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(login, animated: true)
    }
    
}

//MARK: CollectionView Delegate/Datasource
extension TourVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.theCurrentModel.model.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TourCollectionCell", for: indexPath) as! TourCollectionCell
        
        let dict = self.theCurrentModel.model[indexPath.row]

        cell.lblTitle.text = dict.name
        cell.lblTitle.text = dict.description
        cell.imgTitle.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgTitle?.sd_setImage(with: dict.imageUrl?.toURL(), placeholderImage: nil, options: .lowPriority, context: nil)
        
//        cell.lblTitle.text = dict["title"].stringValue
//        cell.lblDescription.text = dict["description"].stringValue
//        cell.imgTitle.image = UIImage(named: dict["img"].stringValue)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        print("Collection current index :", indexPath.row)
        
        theCurrentModel.currentIndex = indexPath.row
        updatePageControl(index: theCurrentModel.currentIndex)
        if theCurrentModel.currentIndex == 0 {
            btnShowGetStartedButton()
        } else if (theCurrentModel.currentIndex + 1) == theCurrentModel.model.count{
            btnShowFinishButton()
        } else {
            btnShowNextButton()
        }
    }
    
    func redirectToCell(indexPath:IndexPath){
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        theCurrentModel.currentIndex = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
//        redirectToCell(indexPath: IndexPath(item: theCurrentModel.currentIndex, section: 0))
//        updatePageControl(index: theCurrentModel.currentIndex)
//        print("Index: ", theCurrentModel.currentIndex)
//        if theCurrentModel.currentIndex == 0 {
//            btnShowGetStartedButton()
//        } else if (theCurrentModel.currentIndex + 1) == theCurrentModel.model.count{
//            btnShowFinishButton()
//        } else {
//            btnShowNextButton()
//        }
//    }
}
 
