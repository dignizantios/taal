//
//  TourModel.swift
//  Taal
//
//  Created by Vishal on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class TourModel: NSObject {
    
    //MARK:- Variable
    fileprivate weak var theController:TourVC!
    var arrTour:[JSON] = []
    var currentIndex:Int = 0
    var model : [TourDataModel] = []
    
    //MARK:- LifeCycle
    init(theController:TourVC) {
        self.theController = theController
    }
    
    func tourDataApi(completionHandlor:@escaping()->Void) {
        
        let url = screenBase+userAppType
        let parameters = NSDictionary() as! [String : Any]
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        self.theController.showLoader()
        WebServices().MakeGetAPI(name: url, params: parameters, header: header) { (result, error, statusCode) in
        
            self.theController.hideLoader()
            if statusCode == success {
                if let data = result as? NSArray {
                    print("Result:-\(result ?? [])")
                    let modelData = JSON(data)
                    
                    userDefault.set(true, forKey: isFirstTime)
                    
                    for data in modelData.arrayValue {
                        print("Data:-\(data)")
                        let value = TourDataModel(JSON: data.dictionaryObject!)
                        self.model.append(value!)
                    }
                    completionHandlor()
                }
            }
            else if error != nil {
                makeToast(strMessage: error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode ?? 0)
            }
            print("Error:-\(error?.localized ?? "")")
            print("statuscode:-\(statusCode ?? 0)")
        }
    }
}
