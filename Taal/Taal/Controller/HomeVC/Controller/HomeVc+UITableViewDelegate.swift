//
//  HomeVc+UITableViewDelegate.swift
//  Taal
//
//  Created by Abhay on 25/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

extension HomeVC:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.mainModelView.arrayCategoryData.count == 0 {
            setTableView("No_record_found_key".localized, tableView: self.mainView.tblHome)
            return 0
        }
        tableView.backgroundView = nil
        return self.mainModelView.arrayCategoryData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeMainCell") as! HomeMainCell
        
        let dict = self.mainModelView.arrayCategoryData[indexPath.row]
        
        cell.lblTitle.text = dict.name?.capitalized
        cell.imgIcon.sd_setImage(with: dict.imageUrl?.toURL(), completed: nil)
//        cell.btnTitle.setTitle(dict.name, for: .normal)
                
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let categoryVC = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "AddCategoryVC") as! AddCategoryVC
        categoryVC.mainModelView.dictCategoryValue = self.mainModelView.arrayCategoryData[indexPath.row]
        categoryVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(categoryVC, animated: true)
    }
}
