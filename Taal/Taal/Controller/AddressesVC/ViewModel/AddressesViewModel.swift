//
//  AddressesViewModel.swift
//  Taal
//
//  Created by Vishal on 28/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON


class AddressesViewModel {
    
    fileprivate weak var theController : AddressesVC!
    var arrayAllAddress : [AddressDataModel] = []
    var refreshController: UIRefreshControl!
    var enumAddressCheck = enumAddressVcCheck.regular
    var dictAddCaseData = JSON()
    var dicAcceptedData : CategoryDataModel?
    var dicQuotationDetails: QuotationDetails?
    var isCheckEditData = false
    
    
    //MARK: Initializer
    init(theController: AddressesVC) {
        self.theController = theController
    }
}

//MARK: API SetUP
extension AddressesViewModel {
    
    //GetAll Address
    func getAllAddress(completionHandlor:@escaping()->Void) {
        
        let url = getAddressesURL
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        self.theController.showLoader()
        WebServices().MakeGetAPI(name: url, params: [:], header: header) { (response, error, statusCode) in
            
            self.theController.hideLoader()
            self.refreshController.endRefreshing()
            
            if statusCode == success {
                self.arrayAllAddress = []
                if let array = response as? NSArray {
                    let arrayJson = JSON(array).arrayValue
                    print("ArrayJson: ", arrayJson)
                    for data in arrayJson {
                        let value = AddressDataModel(JSON: data.dictionaryObject!)
                        self.arrayAllAddress.append(value!)
                    }
                    self.arrayAllAddress.reverse()
                    completionHandlor()
                }
                else if let dict = response as? NSDictionary {
                    let dictJson = JSON(dict)
                    print("dictJson: ", dictJson)
                }
            }
            else if statusCode == notFound {
                self.arrayAllAddress = []
                makeToast(strMessage: "No_record_found_key".localized)
                completionHandlor()
            }
            else if error != nil {
                print("Error: ", error?.localized ?? "")
            }
        }
    }
    
    //Delete Address
    func deleteAddressApi(addressId: String, completionHandlor:@escaping()->Void) {
        let url = deleteAddressByIdURL+addressId
        print("URL: ", url)
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
                
        self.theController.showLoader()
        WebServices().MakeDeleteAPI(name: url, params: [:], header: header) { (response, error, statusCode) in
        
            print("Response: ", response!)
            self.theController.hideLoader()
            if statusCode == success {
                
                let data = JSON(response!).dictionaryValue
                print("Data: ", data)
                makeToast(strMessage: data["message"]?.stringValue ?? "")
                completionHandlor()
            }
            else if error != nil {
                
                completionHandlor()
            }
        }
    }
    
    func quotationAcceptedAPI(param:[String:Any], completionHandlor:@escaping()->Void) {
        
        let url = quotationAcceptedURL
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        
        print("URL: ", url)
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        self.theController.showLoader()
        WebServices().MakePatchAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            
            if statusCode == success {
                if let dict = response as? NSDictionary {
                    let json = JSON(dict).dictionaryValue
//                    makeToast(strMessage: json["message"]?.stringValue ?? "")
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                completionHandlor()
            }
            else if error != nil {
                makeToast(strMessage: error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode ?? 0)
            }
        }
    }
}

//MARK: AddCase API
extension AddressesViewModel {
    
    func addCaseAPI(param: [String:Any], completionHandlor:@escaping()->Void) {
        
        let url = addCaseURL
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        print("URL: ", url)
        print("PARAM: ", param)
//        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        self.theController.showLoader()
        WebServices().MakePostAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            
            if statusCode == success {
                
                if let data = response {
                    let jsonData = JSON(data).dictionaryValue
//                    makeToast(strMessage: jsonData["message"]!.stringValue)
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                
            }
            else if error != nil {
                makeToast(strMessage: error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode!)
            }
        }
    }
    
    func updateCaseAPI(caseID: String, param: [String:Any], completionHandlor:@escaping()->Void) {
        
        let url = updateCaseURL+caseID
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        print("URL: ", url)
        print("PARAM: ", JSON(param))
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else { 
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        self.theController.showLoader()
        WebServices().MakePutJsonEncodingAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            
            if statusCode == success {
                
                if let dict = response as? NSDictionary {
                    let jsonDict = JSON(dict)
                    print("JsonDict: ", jsonDict)
                    makeToast(strMessage: jsonDict["message"].stringValue)
                    completionHandlor()
                }
//                if let data = response {
//                    let jsonData = JSON(data).dictionaryValue
//                    makeToast(strMessage: jsonData["message"]!.stringValue)
//                }
//                completionHandlor()
            }
            else if statusCode == notFound {
                
            }
            else if error != nil {
                makeToast(strMessage: error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode!)
            }
        }
    }
}
