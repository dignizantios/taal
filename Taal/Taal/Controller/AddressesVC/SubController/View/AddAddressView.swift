//
//  AddAddressView.swift
//  Taal
//
//  Created by Abhay on 29/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import DropDown

class AddAddressView:UIView{
    //MARK: Outlets
    @IBOutlet weak var lblAddressName:UILabel!
    @IBOutlet weak var txtAddressName: UITextField!
    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var lblGovernorate:UILabel!
    @IBOutlet weak var txtGovernorate: UITextField!
    @IBOutlet weak var lblBlock: UILabel!
    @IBOutlet weak var txtBlock: UITextField!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var lblStreet: UILabel!
    @IBOutlet weak var txtStreet: UITextField!
    @IBOutlet weak var lblAvenu: UILabel!
    @IBOutlet weak var txtAvenu: UITextField!
    @IBOutlet weak var lblBuilding: UILabel!
    @IBOutlet weak var txtBuilding: UITextField!
    @IBOutlet weak var lblFloor: UILabel!
    @IBOutlet weak var txtFloor: UITextField!
    @IBOutlet weak var lblExtraNotes: UILabel!
    @IBOutlet weak var txViewExtraNotes: UITextView!
    @IBOutlet weak var btnSubmit: CustomButton!
    
    @IBOutlet weak var lblAreaName: UILabel!
    @IBOutlet weak var txtAreaName: UITextField!
    
    
    func setUpUI(theController: AddAddressVC) {
        [lblAddressName,lblGovernorate,lblBlock,lblCity,lblStreet,lblAvenu,lblBuilding,lblFloor,lblExtraNotes, lblCountryName, lblAreaName].forEach { (lbl) in
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        
        [txtAddressName,txtGovernorate,txtBlock,txtCity,txtStreet,txtAvenu,txtBuilding,txtFloor, txtCountry, txtAreaName].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .regular)
            txt?.delegate = theController
        }
        txViewExtraNotes.font = themeFont(size: 16, fontname: .regular)
        btnSubmit.setTitle("Submit".localized, for: .normal)
        
        lblAddressName.text = "ADDRESS NAME".localized.uppercased()
        lblCountryName.text = "COUNTRY".localized.uppercased()
        lblGovernorate.text = "GOVERNORATE".localized.uppercased()
        lblCity.text = "CITY".localized.uppercased()
        lblAreaName.text = "AREA".localized.uppercased()
        lblBlock.text = "BLOCK".localized.uppercased()
        lblStreet.text = "STREET".localized.uppercased()
        lblAvenu.text = "AVENUE".localized.uppercased()
        lblBuilding.text = "BUILDING".localized.uppercased()
        lblFloor.text = "FLOOR".localized.uppercased()
        lblExtraNotes.text = "EXTRANOTES".localized.uppercased()
                
        /*
        theController.mainViewModel.arrayCityName = ["Ahemdabad","Baroda","Surat","Navsari","Mumbai"]
        theController.mainViewModel.cityNameDropDown.customCellConfiguration = { index, string, cell in
            cell.optionLabel.textAlignment = .left
        }
        theController.mainViewModel.cityNameDropDown.dataSource = theController.mainViewModel.arrayCityName
        theController.configuDropDown(dropDown: theController.mainViewModel.cityNameDropDown, sender: txtCity)
        theController.mainViewModel.cityNameDropDown.selectionAction = { (index, item) in
            self.txtCity.text = item
            theController.view.endEditing(true)
            theController.mainViewModel.cityNameDropDown.hide()
        }
 */
    }
    
    func setUPData(theController: AddAddressVC, data: AddressDataModel) {
        print("Data:-", data)
        
        var country = ""
        theController.mainViewModel.arrayAllCountries.forEach { (dataCountry) in
            if dataCountry.id ?? 0 == data.countryId ?? 0 {
                country = dataCountry.name
                theController.mainViewModel.countryID = data.countryId ?? 0
                
                self.txtCountry.text = country
            }
            
            var zone = ""
            theController.mainViewModel.arrayAllZones.forEach { (dataZone) in
                if dataZone.id ?? 0 == data.zoneId ?? 0 {
                    zone = dataZone.name
                    theController.mainViewModel.zoneID = data.zoneId ?? 0
                    
                    self.txtGovernorate.text = zone
                }
                
                var city = ""
                theController.mainViewModel.arrayAllCities.forEach { (dataCity) in
                    if dataCity.id ?? 0 == data.cityId ?? 0 {
                        city = dataCity.name
                        theController.mainViewModel.cityID = data.cityId ?? 0
                        
                        self.txtCity.text = city
                    }
                }
            }
            
        }
        
        self.txtAddressName.text = data.name
        self.txtBlock.text = data.block
        self.txtStreet.text = data.street
        self.txtAvenu.text = data.area
        self.txtBuilding.text = data.building
        self.txtFloor.text = data.floor
        self.txViewExtraNotes.text = data.notes
        self.txtAreaName.text = data.areaName
    }
}
