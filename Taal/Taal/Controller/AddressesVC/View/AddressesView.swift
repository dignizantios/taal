//
//  AddressesView.swift
//  Taal
//
//  Created by Vishal on 28/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

class AddressesView: UIView {
    
    //MARK: Outlets
    @IBOutlet var tblAddresses: UITableView!

    
    func setUpUI(theController: AddressesVC) {
        
        registerXib()
    }
    
    func registerXib() {
        tblAddresses.register(UINib(nibName: "AddressesTableCell", bundle: nil), forCellReuseIdentifier: "AddressesTableCell")
    }
    
}
