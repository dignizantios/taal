//
//  ImgAddCollectionCell.swift
//  Taal
//
//  Created by Vishal on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class ImgAddCollectionCell: UICollectionViewCell {

    //MARK: Outlets
    @IBOutlet weak var btnImgAdd: UIButton!
    @IBOutlet weak var vwImg: UIView!
    @IBOutlet weak var imgAdd: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgAdd.isHidden = true
        btnDelete.isHidden = true
        self.vwImg.isHidden = true
    }

}
