//
//  AddControllerView.swift
//  Taal
//
//  Created by Vishal on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class AddCategoryView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var tblAddCategory: UITableView!
    @IBOutlet weak var vwTableHeader: UIView!
    @IBOutlet weak var vwTableFooter: UIView!
    @IBOutlet weak var imgCategoryType: UIImageView!
    @IBOutlet weak var lblCategoryType: UILabel!
    @IBOutlet weak var lblSubmitRqst: UILabel!
    @IBOutlet weak var lblSubmitRqstText: UILabel!
    @IBOutlet weak var lblBottomRequiredFields: UILabel!
    @IBOutlet weak var btnSubmit: CustomButton!
    
    
    func setUpUI(theController: AddCategoryVC) {
        
        self.vwMain.isHidden = true
        self.tblAddCategory.tableHeaderView = self.vwTableHeader
        self.tblAddCategory.tableFooterView = self.vwTableFooter
        self.registerXIb()
        btnSubmit.setTitle("Submit".localized, for: .normal)
        lblSubmitRqst.text = "Submit your request".localized
//        lblSubmitRqstText.text = "Submit your request now so providers can offer a good deal!.".localized
        lblSubmitRqstText.text = "Submit your request now so providers can offer a good qoute.".localized
        lblBottomRequiredFields.text = "Fields that marked with (*) are require fields".localized
    }
    
    func setUpData(data: CategoryDataModel) {
        
        self.vwMain.isHidden = false
        self.lblCategoryType.text = (data.name!).capitalized
        self.imgCategoryType.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imgCategoryType.sd_setImage(with: data.imageUrl?.toURL(), placeholderImage: UIImage(named: "ic_request_page_splash_holder"), options: .lowPriority, context: nil)
    }
    
    
    func setUpEditData(data: Category) {
        self.lblCategoryType.text = (data.name!).capitalized
        self.imgCategoryType.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imgCategoryType.sd_setImage(with: data.image?.toURL(), placeholderImage: UIImage(named: "ic_request_page_splash_holder"), options: .lowPriority, context: nil)
    }
    
    func registerXIb() {
        
        self.tblAddCategory.register(UINib(nibName: "CheckMarkTable", bundle: nil), forCellReuseIdentifier: "CheckMarkTable")
        self.tblAddCategory.register(UINib(nibName: "DropdownTableCell", bundle: nil), forCellReuseIdentifier: "DropdownTableCell")
        self.tblAddCategory.register(UINib(nibName: "ImgCollectionTableCell", bundle: nil), forCellReuseIdentifier: "ImgCollectionTableCell")
        self.tblAddCategory.register(UINib(nibName: "AudioTableCell", bundle: nil), forCellReuseIdentifier: "AudioTableCell")
        self.tblAddCategory.register(UINib(nibName: "txtFieldTableCell", bundle: nil), forCellReuseIdentifier: "txtFieldTableCell")
        self.tblAddCategory.register(UINib(nibName: "GenderTableCell", bundle: nil), forCellReuseIdentifier: "GenderTableCell")
    }
}
