//
//  ImgCollectionTableCell.swift
//  Taal
//
//  Created by Vishal on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class ImgCollectionTableCell: UITableViewCell {

    @IBOutlet weak var lblImagesHeader: UILabel!
    @IBOutlet weak var imgAddCollection: UICollectionView!
    @IBOutlet weak var collectionHeightConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblImagesHeader].forEach { (lbl) in
            lbl?.textColor = UIColor.lightGray
            lbl?.font = themeFont(size: 17, fontname: .regular)
        }
        
        self.imgAddCollection.register(UINib(nibName: "ImgAddCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ImgAddCollectionCell")
        self.imgAddCollection.register(UINib(nibName: "SelectRoomCollectionCell", bundle: nil), forCellWithReuseIdentifier: "SelectRoomCollectionCell")
        
        imgAddCollection.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    override func draw(_ rect: CGRect) {
        self.contentView.layoutIfNeeded()
        self.contentView.layoutSubviews()
    }
    
    //MARK:- Overide Method
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UICollectionView {
            print("contentSize:= \(imgAddCollection.contentSize.height)")
            self.contentView.layoutIfNeeded()
            self.contentView.layoutSubviews()
            self.collectionHeightConstraint.constant = imgAddCollection.contentSize.height
        }
    }
    
    deinit {
        self.imgAddCollection.removeObserver(self, forKeyPath: "contentSize")
    }
    
}
