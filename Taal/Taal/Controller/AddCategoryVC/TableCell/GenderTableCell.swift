//
//  GenderTableCell.swift
//  Taal
//
//  Created by Vishal on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class GenderTableCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var lblFemale: UILabel!
    var handlorTxtValue:(_ index: Int, _ value: String) -> Void = { _, _ in }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblTitle].forEach { (lbl) in
            
            lbl?.textColor = UIColor.lightGray
            lbl?.font = themeFont(size: 17, fontname: .regular)
        }
        
        [lblMale, lblFemale].forEach { (lbl) in
            lbl?.textColor = UIColor.black
            lbl?.font = themeFont(size: 17, fontname: .regular)
        }
        
    }
    
    @IBAction func btnMaleAction(_ sender: Any) {
        
        if self.btnMale.isSelected == true {
//            self.btnMale.isSelected = false
        }
        else {
            handlorTxtValue(btnMale.tag, "Male")
            self.btnMale.isSelected = true
            self.btnFemale.isSelected = false
        }
    }
    
    @IBAction func btnFemaleAction(_ sender: Any) {
        
        if self.btnFemale.isSelected == true {
//            self.btnFemale.isSelected = false
        }
        else {
            handlorTxtValue(btnMale.tag, "Female")
            self.btnFemale.isSelected = true
            self.btnMale.isSelected = false
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setUpData(data: CategoryDataModel) {
        
        if data.editValue?.count ?? 0 > 0 {
            btnMale.isSelected = data.editValue?.first ?? "" == "Male" ? true : false
            btnFemale.isSelected = data.editValue?.first ?? "" == "Female" ? true : false
        }
    }
}
