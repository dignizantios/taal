//
//  AddControllerVC.swift
//  Taal
//
//  Created by Vishal on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddCategoryVC: UIViewController {
    
    //MARK: Initializer
    lazy var mainView: AddCategoryView = { [unowned self] in
        return self.view as! AddCategoryView
        }()
    
    lazy var mainModelView: AddCategoryViewModel = {
        return AddCategoryViewModel(theController: self)
    }()
    var selectedRadioIndex = [Int]()
    var collectionTag = Int()
    var selectImageCollectionTag = Int()
    var tableTag = Int()
    var checkBoxTableTag = Int()
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI(theController: self)
        setupTransparontsNavigationBar()
        
        if self.mainModelView.enumAddCase == .editCase {
            self.mainView.vwMain.isHidden = false
            self.mainModelView.editCaseGetData(caseId: "\(self.mainModelView.dictCategoryValue?.caseDetails?.caseId ?? 0)") {
                print("Get edit dta")
                if self.mainModelView.dictCategoryValue?.caseDetails?.category != nil {
                    self.mainView.setUpEditData(data: (self.mainModelView.dictCategoryValue?.caseDetails!.category)!)
                }
                self.mainView.tblAddCategory.reloadData()
            }
        }
        else {
            if self.mainModelView.dictCategoryValue!.id != nil {
                self.mainView.vwMain.isHidden = false
                self.mainModelView.getCaseParameters(categoryID: "\(self.mainModelView.dictCategoryValue!.id ?? 0)") {
                    if self.mainModelView.arrayCaseData.count > 0 {
//                        self.dropDownSetup(sender: UIButton())
                        self.mainView.tblAddCategory.reloadData()
                    }
                    self.mainView.setUpData(data: self.mainModelView.dictCategoryValue!)
                }
            }
        }
    }    
    
    override func viewDidLayoutSubviews() {        
        
    }
    
    
    //MARK: Button Action
    @IBAction func btnSubmitAction(_ sender: Any) {
        self.view.endEditing(true)
        
        if self.mainModelView.enumAddCase == .editCase {
            
            for data in self.mainModelView.arrayCaseData {
                if data.valuePassed == false && data.required == true {
                    makeToast(strMessage: "Please_fill_required_fields_key".localized)
                    return
                }
                else {
                    if data.type == "image" && data.valuePassed == true {
                        self.showLoader()
                        var txtData = JSON()
                        txtData["id"].intValue = Int(truncating: data.id!)
                        txtData["type"].stringValue = data.type!
                        var arrayValue : [String] = []
                        for i in data.arrayImage {
                            do {
                                let imageData:NSData = i.pngData()! as NSData
                                let imageStr = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                                arrayValue.append(imageStr)
                            }
                        }
                        txtData["value"].arrayObject = JSON(arrayValue).arrayObject
                        self.mainModelView.arrData.append(txtData)
                    }
                    self.hideLoader()
                }
            }
//            "\(self.mainModelView.dictCategoryValue?.caseDetails?.caseId ?? 0)"
            self.mainModelView.dictAddCase["category_id"].stringValue = "\(self.mainModelView.dictCategoryValue?.caseDetails?.caseId ?? 0)"
            self.mainModelView.dictAddCase["fields"] = JSON(self.mainModelView.arrData)
            
            let addressVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "AddressesVC") as! AddressesVC
            addressVC.mainViewModel.dictAddCaseData = self.mainModelView.dictAddCase
            //        print("Array Data: ", addressVC.mainViewModel.dictAddCaseData)
            addressVC.mainViewModel.enumAddressCheck = .isEditCase
            self.navigationController?.pushViewController(addressVC, animated: true)
        }
        else {
            for data in self.mainModelView.arrayCaseData {
                if data.valuePassed == false && data.required == true {
                    makeToast(strMessage: "Please_fill_required_fields_key".localized)
                    return
                }
                else {
                    if (data.type == "image" || data.type == "images") && data.valuePassed == true {
                        self.showLoader()
                        var txtData = JSON()
                        txtData["id"].intValue = Int(truncating: data.id!)
                        txtData["type"].stringValue = data.type!
                        var arrayValue : [String] = []
                        for i in data.arrayImage {
                            do {
                                let imageData:NSData = i.pngData()! as NSData
                                let imageStr = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                                arrayValue.append(imageStr)
                            }
                        }
                        txtData["value"].arrayObject = JSON(arrayValue).arrayObject
                        self.mainModelView.arrData.append(txtData)
                    }
                    self.hideLoader()
                }
            }
            self.mainModelView.dictAddCase["category_id"].intValue = Int(truncating: self.mainModelView.dictCategoryValue!.id!)
            self.mainModelView.dictAddCase["fields"] = JSON(self.mainModelView.arrData)
            
            let addressVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "AddressesVC") as! AddressesVC
            addressVC.mainViewModel.dictAddCaseData = self.mainModelView.dictAddCase
            //        print("Array Data: ", addressVC.mainViewModel.dictAddCaseData)
            addressVC.mainViewModel.enumAddressCheck = .addCase
            self.navigationController?.pushViewController(addressVC, animated: true)
        }
    }

    func dropDownSetup(sender: UIButton, dataSource: [String]) {
        
        self.mainModelView.selectionDropDown.customCellConfiguration = { index, string, cell in
            cell.optionLabel.textAlignment = .left
        }
        self.mainModelView.selectionDropDown.dataSource = dataSource
        
        self.configuDropDown(dropDown: self.mainModelView.selectionDropDown, sender: sender)
        
        self.mainModelView.selectionDropDown.selectionAction =  { (index, item) in
//            self.view.endEditing(true)
            self.mainModelView.handlorDropDown(sender.tag, item)
            sender.setTitle(item, for: .normal)
            self.mainModelView.selectionDropDown.hide()
        }
    }
}

