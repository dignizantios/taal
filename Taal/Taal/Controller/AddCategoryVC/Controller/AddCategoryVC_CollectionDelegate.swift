//
//  AddCategoryVC_CollectionDelegate.swift
//  Taal
//
//  Created by Vishal on 16/01/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import YangMingShan

extension AddCategoryVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        
        if self.mainModelView.arrayCaseData[collectionView.tag].type == "radio-group" {
            return self.mainModelView.arrayCaseData[collectionView.tag].value?.count ?? 0
        }
        else {
            if self.mainModelView.arrayCaseData[self.tableTag].arrayImage.count == 0 {
                return self.mainModelView.arrayCaseData[self.tableTag].arrayImage.count+4
            } else if self.mainModelView.arrayCaseData[self.tableTag].arrayImage.count == 1 {
                return self.mainModelView.arrayCaseData[self.tableTag].arrayImage.count+3
            }
            else if self.mainModelView.arrayCaseData[self.tableTag].arrayImage.count == 2 {
                return self.mainModelView.arrayCaseData[self.tableTag].arrayImage.count+2
            }
            else {
                return self.mainModelView.arrayCaseData[self.tableTag].arrayImage.count+1
            }
        }
//        return self.mainModelView.arrayCaseData[collectionView.tag].arrayImage.count+4
//        return self.mainModelView.arrayImages.count+1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.mainModelView.arrayCaseData[collectionView.tag].type == "radio-group" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectRoomCollectionCell", for: indexPath) as! SelectRoomCollectionCell
            
            let title = self.mainModelView.arrayCaseData[collectionView.tag].value?[indexPath.row]
            cell.btnTotalRoom.setTitle(" \(title ?? "") ", for: .normal)
            cell.btnTotalRoom.tag = indexPath.row
            
            var txtData = JSON()
            collectionTag = collectionView.tag
            
            if self.mainModelView.arrayCaseData[collectionView.tag].editValue?.count ?? 0 > 0 {
                    
                let filter = self.mainModelView.arrData.filter { (data) -> Bool in
                    let data = data["id"].int == Int(truncating: self.mainModelView.arrayCaseData[self.collectionTag].id!) ? true : false
                    return data
                }
                
                if filter.count == 0 {
                    txtData["id"].int = Int(truncating: self.mainModelView.arrayCaseData[collectionView.tag].id!)
                    txtData["type"].stringValue = self.mainModelView.arrayCaseData[collectionView.tag].type!
                    txtData["value"] = [self.mainModelView.arrayCaseData[collectionView.tag].editValue!.first!]
                    self.mainModelView.arrData.append(txtData)
                }
            }
            
            cell.currentCollection = collectionView
            if selectedRadioIndex.count > 0 {
                if selectedRadioIndex[0] == indexPath.row {
                    cell.btnTotalRoom.isSelected = true
                }
                else {
                    cell.btnTotalRoom.isSelected = false
                }
            }
            else {
                cell.btnTotalRoom.isSelected = false
                if self.mainModelView.arrayCaseData[collectionView.tag].editValue?.count ?? 0 > 0 {
                    if self.mainModelView.arrayCaseData[collectionView.tag].editValue!.first! == title {
                        cell.btnTotalRoom.isSelected = true
                    }
                }
            }
            
            cell.radioHandlor = { selectedIndex, a, collection in
                self.selectedRadioIndex = [selectedIndex]
                cell.currentCollection.reloadData()

                self.mainModelView.arrayCaseData[collection.tag].valuePassed = true
                
                let value : String = self.mainModelView.arrayCaseData[collection.tag].value?[selectedIndex] ?? ""
                
                let filter = self.mainModelView.arrData.filter { (data) -> Bool in
                    let data = data["id"].int == Int(truncating: self.mainModelView.arrayCaseData[collection.tag].id!) ? true : false
                    return data
                }
                for i in 0..<self.mainModelView.arrData.count {
                    if self.mainModelView.arrData[i]["id"].int == Int(truncating: self.mainModelView.arrayCaseData[collection.tag].id!) {
                        self.mainModelView.arrData[i]["value"] = [value]
                    }
                }
                if filter.count == 0 {
                    txtData["id"].int = Int(truncating: self.mainModelView.arrayCaseData[collection.tag].id!)
                    txtData["type"].stringValue = self.mainModelView.arrayCaseData[collection.tag].type!
                    txtData["value"] = [value]
                    self.mainModelView.arrData.append(txtData)
                }
            }
            
            return cell
        }
        else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImgAddCollectionCell", for: indexPath) as! ImgAddCollectionCell
            
            if self.mainModelView.arrayCaseData[collectionView.tag].arrayImage.count > indexPath.row {
                let dict = self.mainModelView.arrayCaseData[collectionView.tag].arrayImage[indexPath.row]
                cell.imgAdd.image = dict
                cell.vwImg.isHidden = false
                cell.vwImg.isHidden = false
                cell.imgAdd.isHidden = false
                cell.btnDelete.isHidden = false
            }
            else {
                cell.imgAdd.isHidden = true
                cell.btnDelete.isHidden = true
                cell.vwImg.isHidden = true
            }
            
            cell.btnImgAdd.accessibilityValue = "\(collectionView.tag)"
            cell.btnDelete.accessibilityValue = "\(collectionView.tag)"
            cell.btnImgAdd.tag = collectionView.tag
            cell.btnDelete.tag = indexPath.row
            
            if self.mainModelView.arrayCaseData[collectionView.tag].editValue?.count ?? 0 > 0 {
                self.mainModelView.arrayCaseData[collectionView.tag].valuePassed = true
            }
            
            self.mainModelView.handlorDeleteImage = { collectionTag in
                if self.mainModelView.arrayCaseData[collectionTag].arrayImage.count == 0 {
                    self.mainModelView.arrayCaseData[collectionTag].valuePassed = true
                    self.mainModelView.arrayCaseData[collectionTag].arrayImage = []
                    self.mainView.tblAddCategory.reloadRows(at: [IndexPath(row: self.tableTag, section: 0)], with: .none)
                }
                else {
                    if self.mainModelView.arrayCaseData[collectionTag].arrayImage.count > 0 {
                        self.mainModelView.arrayCaseData[collectionTag].valuePassed = true
                    }
                    else {
                        self.mainModelView.arrayCaseData[collectionTag].valuePassed = false
                    }
                    UIView.performWithoutAnimation {
                        self.mainView.tblAddCategory.reloadRows(at: [IndexPath(row: self.tableTag, section: 0)], with: .none)
//                        self.mainView.tblAddCategory.reloadData()
                    }
                }
            }
            
            cell.btnImgAdd.addTarget(self, action: #selector(btnImageSelectionAction(_:)), for: .touchUpInside)
            cell.btnDelete.addTarget(self, action: #selector(btnImageDeleteAction(_:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if self.mainModelView.arrayCaseData[collectionView.tag].type == "radio-group" {
            if self.mainModelView.arrayCaseData[collectionView.tag].value?.count ?? 0 < 3 {
                return CGSize(width: collectionView.frame.width/2, height: 20)
            }
            else {
                return CGSize(width: collectionView.frame.width/4, height: 20)
            }
        }
        else {
            return CGSize(width: collectionView.frame.width/4, height: 100)
        }
    }
    
}

//MARK: Button action
extension AddCategoryVC {
    
    //--- Image Selected
    @objc func btnImageSelectionAction(_ sender: UIButton) {
        
        let pickerViewController = YMSPhotoPickerViewController.init()
        pickerViewController.numberOfPhotoToSelect = 10
        
        let pcker = YMSPhotoPickerViewController()
        pcker.accessibilityValue = sender.accessibilityValue
        pickerViewController.accessibilityValue = sender.accessibilityValue
        let customColor = UIColor.appThemeBlueColor
        let customCameraColor = UIColor.appThemeBlueColor

        pickerViewController.theme.titleLabelTextColor = UIColor.white
        pickerViewController.theme.navigationBarBackgroundColor = customColor
        pickerViewController.theme.tintColor = UIColor.white
        pickerViewController.theme.orderTintColor = customCameraColor
        pickerViewController.theme.cameraVeilColor = customCameraColor
        pickerViewController.theme.cameraIconColor = UIColor.white
        pickerViewController.theme.statusBarStyle = .lightContent
        
        self.mainModelView.handlorAddImage = {

            self.tableTag = Int(sender.accessibilityValue ?? "") ?? 0
            if self.mainModelView.arrayCaseData[sender.tag].arrayImage.count > 0 {
                self.mainModelView.arrayCaseData[sender.tag].valuePassed = true
            }
            
            UIView.performWithoutAnimation {
//                self.mainView.tblAddCategory.reloadData()
                self.mainView.tblAddCategory.reloadRows(at: [IndexPath(row: self.tableTag, section: 0)], with: .none)
            }
        }
        
        self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
    }
    
    //--- Image Delete
    @objc func btnImageDeleteAction(_ sender: UIButton) {
        
        let mutableImages: NSMutableArray! = NSMutableArray.init(array: self.mainModelView.arrayCaseData[Int(sender.accessibilityValue!) ?? 0].arrayImage)
        mutableImages.removeObject(at: sender.tag)
        self.mainModelView.arrayCaseData[Int(sender.accessibilityValue!) ?? 0].arrayImage = mutableImages.copy()  as? NSArray as! [UIImage]
//        self.mainModelView.arrayCaseData[Int(sender.accessibilityValue!) ?? 0].arrayImage = NSArray.init(array: mutableImages) as? NSArray as! [UIImage]
        UIView.performWithoutAnimation {
//                self.mainView.tblAddCategory.reloadData()
            self.mainView.tblAddCategory.reloadRows(at: [IndexPath(row: Int(sender.accessibilityValue!) ?? 0, section: 0)], with: .none)
        }
        self.mainModelView.handlorDeleteImage(Int(sender.accessibilityValue ?? "0") ?? 0)
        
        
//        let mutableImages: NSMutableArray! = NSMutableArray.init(array: self.mainModelView.arrayImages)
//        mutableImages.removeObject(at: sender.tag)
//        self.mainModelView.arrayImages = NSArray.init(array: mutableImages)
//        self.mainModelView.handlorDeleteImage()
    }
    
}

//MARK: Image selection
extension AddCategoryVC : YMSPhotoPickerViewControllerDelegate {
    
    
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        
        let alertController = UIAlertController(title: "Allow photo album access?", message: "Need your permission to access photo albums", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)

        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func photoPickerViewControllerDidReceiveCameraAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        
        let alertController = UIAlertController(title: "Allow camera album access?", message: "Need your permission to take a photo", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)

        // The access denied of camera is always happened on picker, present alert on it to follow the view hierarchy
        picker.present(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPickingImages photoAssets: [PHAsset]!) {
        // Remember images you get here is PHAsset array, you need to implement PHImageManager to get UIImage data by yourself
        picker.dismiss(animated: true) {
            let imageManager = PHImageManager.init()
            let options = PHImageRequestOptions.init()
            options.deliveryMode = .highQualityFormat
            options.resizeMode = .exact
            options.isSynchronous = true
            
            var mutableImages: NSMutableArray! = []
            if self.mainModelView.arrayCaseData[Int(picker.accessibilityValue!) ?? 0].arrayImage.count > 0 {
                for img in self.mainModelView.arrayCaseData[Int(picker.accessibilityValue!) ?? 0].arrayImage {
                    mutableImages.add(img)
                }
            }
            else {
                mutableImages = []
            }
            
//            if self.mainModelView.arrayImages.count > 0 {
//                for img in self.mainModelView.arrayImages {
//                    mutableImages.add(img)
//                }
//            }
//            else {
//                mutableImages = []
//            }
            
//            let mutableImages: NSMutableArray! = []
            for asset: PHAsset in photoAssets {
                let targetSize = CGSize(width: 250, height: 250)
                imageManager.requestImage(for: asset, targetSize: targetSize, contentMode: .default, options: options) { (image, info) in
                    
                    mutableImages.add(image!)
//                    self.mainModelView.arrayImages.add(image!)
                }
            }
            self.mainModelView.arrayCaseData[Int(picker.accessibilityValue!) ?? 0].arrayImage = mutableImages.copy()  as? NSArray as! [UIImage]
//            self.mainModelView.arrayImages = mutableImages.copy() as? NSArray
            self.mainModelView.handlorAddImage()
        }
    }
}
