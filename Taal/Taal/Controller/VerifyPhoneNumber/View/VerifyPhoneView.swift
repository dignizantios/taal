//
//  VerifyPhoneView.swift
//  Taal
//
//  Created by Vishal on 23/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

class VerifyPhoneView: UIView {
    
    //MARK: Outlets
    @IBOutlet var vwMain: UIView!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var lblVerifyPhoneNumber: UILabel!
    @IBOutlet var lblEnter4digitCode: UILabel!
    @IBOutlet var vwOTP: VPMOTPView!
    @IBOutlet var btnContinue: CustomButton!
    @IBOutlet var btnResendCode: CustomButton!
    
    //MARK: SetUp
    
    func setUpUI(theDelegate:VerifyPhoneVC) {
        
        lblVerifyPhoneNumber.font = themeFont(size: 26.0, fontname: .regular)
        lblVerifyPhoneNumber.textColor = UIColor.black
        lblVerifyPhoneNumber.text = "Verify_phone_number_key".localized
        
        lblEnter4digitCode.font = themeFont(size: 16.0, fontname: .regular)
        lblEnter4digitCode.textColor = UIColor.black
        let code = "Enter_6_digit_code_send_to_at_key".localized
//        lblEnter4digitCode.text = "\(code) +96550922558"
        let mobile = "\(theDelegate.mainModelView.signupDataDict["country_code"].stringValue)"+" \(theDelegate.mainModelView.signupDataDict["mobile"].stringValue)"
        lblEnter4digitCode.text = "\(code) \(mobile)"
        
//        vwOTP.transform = CGAffineTransform(translationX: -1, y: 1)
        
        btnBack.setImage(UIImage(named: "ic_back")?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        btnContinue.setTitle("Continue_key".localized, for: .normal)
        btnResendCode.setTitle("Resend_Code_key".localized, for: .normal)
        
        [btnContinue].forEach { (btn) in
            btn?.setTitleColor(UIColor.white, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 18.0, fontname: .regular)
            btn?.layer.cornerRadius = 6
        }
        [btnResendCode].forEach { (btn) in
            btn?.setTitleColor(UIColor.black, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 18.0, fontname: .regular)
            btn?.layer.cornerRadius = 6
        }
        vwOTP.otpFieldsCount = 6
        vwOTP.otpFieldDefaultBorderColor = UIColor.lightGray
        vwOTP.otpFieldEnteredBorderColor = UIColor.lightGray
        vwOTP.otpFieldErrorBorderColor = UIColor.red
        vwOTP.otpFieldBorderWidth = 0.5
        vwOTP.delegate = theDelegate.self
        vwOTP.otpFieldDisplayType = .square
        vwOTP.cursorColor = UIColor.lightGray
        vwOTP.shouldAllowIntermediateEditing = false
        vwOTP.otpFieldFont = themeFont(size: 15, fontname: .regular)
        
        if UI_USER_INTERFACE_IDIOM() == .phone {
            let result = UIScreen.main.bounds.size
            if result.height == 480 || result.height == 568 || result.height == 844 {
                vwOTP.otpFieldSize = 33
            }else{
                vwOTP.otpFieldSize = 45
            }
            
            if result.width <= 375 {
                vwOTP.otpFieldSeparatorSpace = 10
            } else {
                vwOTP.otpFieldSeparatorSpace = 20
            }
        }
        
        DispatchQueue.main.async {
            self.vwOTP.initializeUI()
        }
    }
}
