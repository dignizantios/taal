//
//  ProfileViewModel.swift
//  Taal
//
//  Created by Abhay on 25/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class ProfileViewModel {
    
    //MARK:- Variables
    fileprivate weak var theController:ProfileVC!
    
    //MARK: Initialized
    init(theController:ProfileVC) {
        self.theController = theController
    }
    
    //MARK: Variables
    var arrayHeader : [String] = []
    var arrayImgHeader : [String] = []
    var imagePicker = UIImagePickerController()
    var customImagePicker   = CustomImagePicker()
    
}

//MARK: Api setup
extension ProfileViewModel {
    
    func getUserProfileAPI(completionHandlor: @escaping()->Void) {
        let url = getUserProfileURL
        let param = NSDictionary() as! [String:Any]
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        self.theController.showLoader()
        
        WebServices().MakeGetAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            
            self.theController.hideLoader()
            
            if statusCode == success {
                if let dict = response as? NSDictionary {
                    let jsonDict = JSON(dict)
                    print("JsonDict: ", jsonDict)
                    updateUserDicData(user: jsonDict)
                    completionHandlor()
                }
            }
            else if error != nil {
                print("Error: ", error?.localized ?? "")
            }
            else {
                print(statusCode ?? 0)
            }
        }
    }
    
    func updateUserProfileAPI(param:[String:Any], completionHandlor:@escaping()->Void) {
        
        let url = updateUserProfileURL
        print("Param: ", param)
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        self.theController.showLoader()
        WebServices().MakePutAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            
            if statusCode == success {
                if let dict = response as? NSDictionary {
                    let jsonDict = JSON(dict)
                    print("JsonDict: ", jsonDict)
                    makeToast(strMessage: jsonDict["message"].stringValue)
                    completionHandlor()
                }
            }
            else if error != nil {
                
            }
            else {
                
            }
        }
    }
}

