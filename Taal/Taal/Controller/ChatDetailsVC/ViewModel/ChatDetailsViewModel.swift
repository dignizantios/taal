//
//  ChatDetailsViewModel.swift
//  Taal
//
//  Created by Abhay on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class ChatDetailsViewModel {
    
    //MARK:- Variables
    fileprivate weak var theController:ChatDetailsVC!
    
    //MARK: Initialized
    init(theController:ChatDetailsVC) {
        self.theController = theController
    }
    
    //MARK: Variables
    var arrayChat : [String] = []
    var isKeyboardOpen = false
    var tableviewTxtviewisClicked = false
    var conversatinID = ""
    var userName = ""
    var arrayChatDetail : [ChatDataModel] = []
    
        
    func setupKeyboard() {
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    //---
    func userChatHistoryApi(conversationID:String, completionHandlor:@escaping()->Void) {
        
        let url = userChatHistoryURL+conversationID
        print("URL: ", url)
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        print("URL: ", url)
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        self.theController.showLoader()
        WebServices().MakeGetAPI(name: url, params: [:], header: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            if statusCode == success {
                if let array = response as? NSArray {
                    let jsonArray = JSON(array).arrayValue
                    print("json array \(jsonArray)")
                    for data in jsonArray {
                        let chatData = ChatDataModel(JSON: data.dictionaryObject!)
                        self.arrayChatDetail.append(chatData!)
                    }
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                
            }
            else if error != nil {
                print("Error: ", error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode ?? 0)
            }
        }
    }
    
    func sendMessageToProviderAPI(param:[String:Any], completionHandlor:@escaping()->Void) {
            
            let url = sendMessageToUserURL
            print("PARAMS: ", param)
            var header : [String:String] = [:]
            header["accept"] = "application/json"
            header["Accept-Language"] = "language".localized
            header["Version"] = iosVersion
            header["X-CSRF-TOKEN"] = ""
            header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
            
    //        self.theController.showLoader()
            
            WebServices().makePostApiWithBody(name: url, params: param, headers: header) { (response, error, statusCode) in
                self.theController.hideLoader()
                if statusCode == success {
                    if response != nil {
                        let data = JSON(response!).dictionaryObject
                        print("Dta: ", data!)
                    }
                    completionHandlor()
                }
                else if error != nil {
                    makeToast(strMessage: error?.localized ?? "")
                }
                else {
                    
                }
            }
        }
}


//MARK: KeyBoard observer method setup
extension ChatDetailsViewModel {
    @objc func keyboardWillShow(_ notification: NSNotification) {
        
        let view = (self.theController.view as? ChatDetailsView)
        // Do something here
        if let keyboardRectValue = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size {
            let keyboardHeight = keyboardRectValue.height
            isKeyboardOpen = true
            if tableviewTxtviewisClicked {
                view!.tblMessageChat.contentInset.bottom = keyboardHeight
                return
            }
            UIView.animate(withDuration: 1.5, animations: {
                var bottomPadding: CGFloat = 0.0
                if #available(iOS 11.0, *) {
                    let window = UIApplication.shared.keyWindow
                    bottomPadding = window?.safeAreaInsets.bottom ?? 0.0
                }
                view?.constantBottomofView.constant = keyboardHeight - bottomPadding
            }, completion: { (status) in
                self.theController.scrollToBottom()
            })
        }
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification) {
        let view = (self.theController.view as? ChatDetailsView)
        isKeyboardOpen = false
        if tableviewTxtviewisClicked {
            tableviewTxtviewisClicked = false
            view!.tblMessageChat.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            return
        }
        UIView.animate(withDuration: 1.5, animations: {
            view?.constantBottomofView.constant = 0.0
            view!.layoutIfNeeded()
        }, completion: nil)
    }
}
