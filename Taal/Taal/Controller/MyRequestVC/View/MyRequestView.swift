//
//  MyRequestView.swift
//  Taal
//
//  Created by Abhay on 25/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import CarbonKit
import UIKit

class MyRequestView: UIView {
    
    //MARK: Outlets
    @IBOutlet var topCollection: UICollectionView!
    @IBOutlet var vwMenu: UIView!
    @IBOutlet weak var btnPending: UIButton!
    @IBOutlet weak var vwBottomBtnPending: UIView!
    @IBOutlet weak var btnAccepted: UIButton!
    @IBOutlet weak var vwBottomBtnAccepted: UIView!
    @IBOutlet weak var tblMyRequest: UITableView!
    
    
    //MARK: SetUp
    func setUpUI(theDelegate: MyRequestVC) {
        
        tblMyRequest.tableFooterView = UIView()
        pendingAction()
        theDelegate.mainModelView.arrayHeader = ["Pending_key".localized.uppercased(), "Accepted_key".localized.uppercased()]
        tblMyRequest.register(UINib(nibName: "PendingTableCell", bundle: nil), forCellReuseIdentifier: "PendingTableCell")
        
        [btnPending, btnAccepted].forEach { (btn) in
            btn?.setTitleColor(UIColor.white, for: .normal)
            btn?.setupThemeButtonUI()
            btn?.titleLabel?.font = themeFont(size: 17, fontname: .boldSemi)
            btn?.layer.cornerRadius = 0
        }
        
        btnPending.setTitle("Pending_key".localized, for: .normal)
        btnAccepted.setTitle("Accepted_key".localized, for: .normal)
        
//        theDelegate.mainModelView.carbonTabSwipeNavegation = CarbonTabSwipeNavigation(items: theDelegate.mainModelView.arrayHeader, delegate: theDelegate)
//
//        setUpCarbonVC(theDelegate: theDelegate)
//
//        theDelegate.mainModelView.carbonTabSwipeNavegation.insert(intoRootViewController: theDelegate, andTargetView: vwMenu)
//        theDelegate.mainModelView.carbonTabSwipeNavegation.view.layoutIfNeeded()
    }
    
    func pendingAction() {
        vwBottomBtnPending.isHidden = false
        vwBottomBtnAccepted.isHidden = true
    }
    
    func acceptedAction() {
        vwBottomBtnPending.isHidden = true
        vwBottomBtnAccepted.isHidden = false
    }
    
    
    func setUpCarbonVC(theDelegate: MyRequestVC) {
        
        let width = theDelegate.view.frame.width
        let tabWidth = (width/CGFloat(theDelegate.mainModelView.arrayHeader.count))
        theDelegate.mainModelView.carbonTabSwipeNavegation.toolbar.isTranslucent = true
        theDelegate.mainModelView.carbonTabSwipeNavegation.carbonSegmentedControl?.backgroundColor = UIColor.appThemeBlueColor
        theDelegate.mainModelView.carbonTabSwipeNavegation.setIndicatorColor(UIColor.appThemeOrangeColor)
        theDelegate.mainModelView.carbonTabSwipeNavegation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 0)
        theDelegate.mainModelView.carbonTabSwipeNavegation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 1)
        theDelegate.mainModelView.carbonTabSwipeNavegation.toolbar.layer.masksToBounds = false
        
        theDelegate.mainModelView.carbonTabSwipeNavegation.toolbarHeight.constant = 50
        theDelegate.mainModelView.carbonTabSwipeNavegation.carbonTabSwipeScrollView.isScrollEnabled = false
        theDelegate.mainModelView.carbonTabSwipeNavegation.setNormalColor(UIColor.white, font: themeFont(size: 17, fontname: .regular))
        theDelegate.mainModelView.carbonTabSwipeNavegation.setSelectedColor(UIColor.white, font: themeFont(size: 17, fontname: .regular))
        
    }
}

