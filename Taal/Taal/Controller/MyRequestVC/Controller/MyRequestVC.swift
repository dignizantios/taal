//
//  MyRequestVC.swift
//  Taal
//
//  Created by Abhay on 25/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class MyRequestVC: UIViewController {
    
    //MARK: Variables
    lazy var mainView: MyRequestView = { [unowned self] in
        return self.view as! MyRequestView
        }()
    
    lazy var mainModelView: MyRequestModel = {
        return MyRequestModel(theController: self)
    }()
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.setUpUI(theDelegate: self)
        if isStartJobNotification {
            self.mainModelView.isShowLoader = false
            self.btnAcceptedAction(self.mainView.btnAccepted)
            let invoiceVC = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "InvoiceVC") as! InvoiceVC
            invoiceVC.hidesBottomBarWhenPushed = true
            invoiceVC.mainModelView.caseID = dictNotificationData["record_id"].stringValue
//            "\(data.caseId ?? 0)"
            self.navigationController?.pushViewController(invoiceVC, animated: false)
        }
        else if isCompletedJobNotification {
            self.mainModelView.isShowLoader = false
            self.btnAcceptedAction(self.mainView.btnAccepted)
            let invoiceVC = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "InvoiceVC") as! InvoiceVC
            invoiceVC.hidesBottomBarWhenPushed = true
            invoiceVC.mainModelView.caseID = dictNotificationData["record_id"].stringValue
            //            "\(data.caseId ?? 0)"
            self.navigationController?.pushViewController(invoiceVC, animated: false)
        }
        else {
            self.mainModelView.isShowLoader = true
            self.mainModelView.getCasesHistoryAPI(statusID: "0") {
                self.mainView.tblMyRequest.reloadData()
            }
            handlorAcceptedQuotation = {
                self.mainModelView.getCasesHistoryAPI(statusID: "0") {
                    self.mainView.tblMyRequest.reloadData()
                }
            }
        }
        setupPullToRefresh()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        if isRequestListVC {
            print("Yse")
            isRequestListVC = false
            self.mainModelView.isShowLoader = true
            self.btnPendingAction(self.mainView.btnPending)
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.mainView.tblMyRequest.reloadData()
        self.mainView.tblMyRequest.layoutIfNeeded()
        self.mainView.tblMyRequest.layoutSubviews()
    }
}

//MARK: SetUpUI
extension MyRequestVC {
    
    func setupPullToRefresh() {
        self.mainModelView.refreshController = UIRefreshControl()
        self.mainModelView.refreshController.backgroundColor = UIColor.clear
        self.mainModelView.refreshController.tintColor = .appThemeBlueColor
        self.mainModelView.refreshController.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        self.mainView.tblMyRequest.addSubview(self.mainModelView.refreshController)
        
    }
    
    @objc func pullToRefresh() {
        self.mainModelView.getCasesHistoryAPI(statusID: self.mainModelView.caseID) {
            self.mainView.tblMyRequest.reloadData()
        }
    }
}

//MARK: Button Action
extension MyRequestVC {
    
    @IBAction func btnPendingAction(_ sender: Any) {
        
        if self.mainModelView.caseID != "0" {
            self.mainModelView.caseID = "0"
            self.mainModelView.isShowLoader = true
            self.mainModelView.getCasesHistoryAPI(statusID: "0") {
                self.mainView.tblMyRequest.reloadData()
            }
        }
        self.mainView.pendingAction()
    }
    
    @IBAction func btnAcceptedAction(_ sender: Any) {
        
        if self.mainModelView.caseID != "1" {
            self.mainModelView.caseID = "1"
            self.mainModelView.isShowLoader = true
            self.mainModelView.getCasesHistoryAPI(statusID: "1") {
                self.mainView.tblMyRequest.reloadData()
            }
        }
        self.mainView.acceptedAction()
    }
}
