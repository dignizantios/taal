//
//  InvoiceViewModel.swift
//  Taal
//
//  Created by Vishal on 29/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class InvoiceViewModel {
    
    //MARK:- Variables
    fileprivate weak var theController:InvoiceVC!
    var enumNavigationTitle = enumAddressVcCheck.addCase
    var isOpenRequestDetails = Bool()
    var dictAddressData = AddressDataModel(JSON: JSON().dictionaryObject!)
    var dicAcceptedData : CategoryDataModel? = nil
    var dicQuotationDetails: QuotationDetails? = nil
    var caseID = ""
    var arrayImages : [Fields] = []
    
    
    
    //MARK: Initialized
    init(theController:InvoiceVC) {
        self.theController = theController
    }
    
    
    func quotationAcceptedAPI(param:[String:Any], completionHandlor:@escaping()->Void) {
        
        let url = quotationAcceptedURL
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        
        print("URL: ", url)
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        self.theController.showLoader()
        WebServices().MakePatchAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            
            if statusCode == success {
                if let dict = response as? NSDictionary {
                    let json = JSON(dict).dictionaryValue
                    makeToast(strMessage: json["message"]?.stringValue ?? "")
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                completionHandlor()
            }
            else if error != nil {
                makeToast(strMessage: error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode ?? 0)
            }
        }
    }
    
    
    func getCaseDetailAPI(caseID: String, completionHandlor:@escaping()->Void) {
        
        let url = getcaseDetailsURL+caseID
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        
        print("URL: ", url)
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        self.theController.showLoader()
        
        WebServices().MakeGetAPI(name: url, params: [:], header: header) { (response, error, statusCode) in
            
            self.theController.hideLoader()
            if statusCode == success {
                if let dict = response as? NSDictionary {
                    let jsonDict = JSON(dict).dictionaryObject!
                    print("JSONDict: ", JSON(dict).dictionaryValue)
                    self.dicAcceptedData = CategoryDataModel(JSON: jsonDict)
                    print("")
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                completionHandlor()
            }
            else if error != nil {
                print("Error: ", error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode ?? 0)
            }
        }
    }
    
    //MARK: API Setup
    func caseChangeStatusAPI(params: [String:Any], completionHandlor:@escaping()->Void) {
        
        let url = caseChangeStatusURL
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        
        print("URL: ", url)
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        self.theController.showLoader()
        WebServices().MakePatchAPI(name: url, params: params, header: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            if statusCode == success {
                if let dict = response as? NSDictionary {
                    let jsonDict = JSON(dict)
                    self.dicAcceptedData?.caseDetails?.status = jsonDict["job_status"].stringValue
                    print("jsonDict: ", jsonDict)
                }
                else if let array = response as? NSArray {
                    print("array: ", array)
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                completionHandlor()
            }
            else if error != nil {
                print("Error: ", error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode ?? 0)
            }
        }
    }
}
