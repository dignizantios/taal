//
//  InvoiceVC.swift
//  Taal
//
//  Created by Vishal on 29/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import AVFoundation

class InvoiceVC: UIViewController {

    //MARK: Variables
    lazy var mainView: InvoiceView = {[unowned self] in
        return self.view as! InvoiceView
        }()
    
    lazy var mainModelView: InvoiceViewModel = {
        return InvoiceViewModel(theController: self)
    }()
    
    var player: AVPlayer!
    var isPause : Bool = false
    var audioURL = ""
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI(theController: self)
        
        if mainModelView.enumNavigationTitle == .acceptedQuotation {
            self.setUpNavigationBarWithTitleRightAndBack(strTitle: "Invoice_key".localized, barColor: UIColor.appThemeBlueColor, showTitle: true, isLeftBackButton: true, isRightSkipButton: false)
            self.mainView.setUpAcceptedQuotationData(categoryData: self.mainModelView.dicAcceptedData!, quotationData: self.mainModelView.dicQuotationDetails!, addressData: self.mainModelView.dictAddressData!)
        }
        else {
            self.setUpNavigationBarWithTitleRightAndBack(strTitle: "Request_detail_key".localized, barColor: UIColor.appThemeBlueColor, showTitle: true, isLeftBackButton: true, isRightSkipButton: false)
            
            self.mainView.vwMain.isHidden = true
            self.mainModelView.getCaseDetailAPI(caseID: self.mainModelView.caseID) {
                self.mainView.vwMain.isHidden = false
                self.mainView.setUpData(categoryData: self.mainModelView.dicAcceptedData!, theController: self)
            }
            self.mainView.bottomView.isHidden = false
            self.mainView.bntConfirm.isHidden = true
            
            if isStartJobNotification {
                isStartJobNotification = false
                let vc = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "TrackingVC") as! TrackingVC
                vc.strConvesationID = dictNotificationData["conversation_id"].stringValue
//                    self.mainModelView.dicAcceptedData?.userDetails?.conversationId ?? ""
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else if isCompletedJobNotification {
                isCompletedJobNotification = false
            }
        }
        
//        if isStartJobNotification {
//            isStartJobNotification = false
//            let vc = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "TrackingVC") as! TrackingVC
//            vc.strConvesationID =  self.mainModelView.dicAcceptedData?.userDetails?.conversationId ?? ""
//            self.navigationController?.pushViewController(vc, animated: false)
//        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mainView.tblDescription.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if isPause {
            self.player.pause()
        }
        self.mainView.tblDescription.removeObserver(self, forKeyPath: "contentSize")
        super.viewWillDisappear(true)
    }
    
    //MARK:- Buton Actions
    @IBAction func btnAudioOnOffAction(_ sender: Any) {
        
        
        if audioURL == "" {
            audioURL = (self.mainModelView.dicAcceptedData?.quotationDetails?.first!.voiceNotes)!
            print("AudioURL: ", audioURL)
            if audioURL != "" {
                self.playSound(urlAudio: audioURL)
            }
        }
        else {
            if isPause {
                isPause = false
                self.player.pause()
                self.mainView.btnAudioOnOff.setImage(UIImage(named: "ic_microphone"), for: .normal)
            }
            else {
                isPause = true
                //ic_voice_white_push
                //ic_voice_white
                self.mainView.btnAudioOnOff.setImage(UIImage(named: "ic_voice_white_push"), for: .normal)
                self.player.play()
            }
        }
    }
    
    func playSound(urlAudio:String) {
        guard  let url = URL(string: urlAudio) else {
            self.audioURL = ""
            print("Invalid URL: ")
            return
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
            try AVAudioSession.sharedInstance().setActive(true)
            player = try AVPlayer(url: url as URL)
            guard let player = player
                else {
                    return
            }
            
            self.isPause = true
            player.play()
            self.mainView.btnAudioOnOff.setImage(UIImage(named: "ic_voice_white_push"), for: .normal)
            setTimerObserver(player)
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func setTimerObserver(_ player: AVPlayer?) {
        
        player?.addPeriodicTimeObserver(forInterval: CMTime.init(value: 1, timescale: 1), queue: .main, using: { (time) in
            if let duration = player?.currentItem?.duration {
                let duration = CMTimeGetSeconds(duration), time = CMTimeGetSeconds(time)
                if !duration.isNaN {
                    
                    self.mainView.timerAction(max: Float(duration), min: Float(time))
                    //
                    let data = secondsToHoursMinutesSeconds(seconds: Int(time))
                    let durationTime = secondsToHoursMinutesSeconds(seconds: Int(duration))
                    print("Hours: ", data.0)
                    print("Minutes: ", data.1)
                    print("Seconds: ", data.2)
                    
                    if data.1 == durationTime.1 && data.2+1 == durationTime.2 {
                        self.mainView.btnAudioOnOff.setImage(UIImage(named: "ic_microphone"), for: .normal)
                        print("Stop")
                    }
                    
                    self.mainView.lblAudioTimer.text = "\(data.1):"+"\(data.2)" + "/\(durationTime.1):\(durationTime.2)"
                    
                }
            }
        })
    }
        
    
    @IBAction func btnCallAction(_ sender: UIButton) {
        
        let mobileNumber = "\(self.mainModelView.dicAcceptedData?.quotationDetails?.first?.provider?.providerCountryCode ?? "")\(self.mainModelView.dicAcceptedData?.quotationDetails?.first?.provider?.providerMobile ?? "")"
        
        dialNumber(number: mobileNumber)
    }
    
    func dialNumber(number : String) {
        
        if let url = URL(string: "tel://\(number)"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else {
            print("Not calling: ")
        }
    }
    
    @IBAction func btnChat(_ sender: UIButton) {
        
        let vc = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "ChatDetailsVC") as! ChatDetailsVC
        vc.mainModelView.userName = self.mainModelView.dicAcceptedData?.userDetails?.name ?? ""
        vc.mainModelView.conversatinID = self.mainModelView.dicAcceptedData?.userDetails?.conversationId ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnTrackNow(_ sender: Any) {
        
//        let param = ["case_id":self.mainModelView.dicAcceptedData?.caseDetails?.caseId ?? 0,
//                     "status":5] as [String : Any]
//
//        self.mainModelView.caseChangeStatusAPI(params: param) {
//            print("Case changed")
//            self.mainView.setUpData(categoryData: self.mainModelView.dicAcceptedData!, theController: self)
//        }
        
        let vc = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "TrackingVC") as! TrackingVC
        vc.strConvesationID =  self.mainModelView.dicAcceptedData?.userDetails?.conversationId ?? ""
        vc.mainViewModel.dictCategoryData = self.mainModelView.dicAcceptedData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnCompletedJobAction(_ sender: Any) {
        
        print("Completed job:")
        let param = ["case_id":self.mainModelView.dicAcceptedData?.caseDetails?.caseId ?? 0,
                     "status":7] as [String : Any]

        self.mainModelView.caseChangeStatusAPI(params: param) {
            print("Case changed")
            self.mainView.setUpData(categoryData: self.mainModelView.dicAcceptedData!, theController: self)
        }
    }
    
    @IBAction func btnConfirmAction(_ sender: Any) {
        
        let vc = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "QuotationAcceptedVC") as! QuotationAcceptedVC
        
        let param = ["case_id":"\(self.mainModelView.dicAcceptedData?.caseDetails?.caseId ?? 0)",
            "provider_id":"\(self.mainModelView.dicQuotationDetails?.provider?.providerId ?? 0)",
            "quotation_id":"\(self.mainModelView.dicQuotationDetails?.id ?? 0)",
            "address_id":"\(self.mainModelView.dictAddressData?.id ?? 0)"]
        
        self.mainModelView.quotationAcceptedAPI(param: param) {
            
            if vc.isCheckController == .addCase {
                vc.isCheckController = .addCase
            }
            else {
                vc.isCheckController = .acceptedQuotation
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    /*var dicAcceptedData : CategoryDataModel? = nil
     var dicQuotationDetails: QuotationDetails? = nil*/
    
    @IBAction func btnAcceptAction(_ sender: Any) {
        
        let addressVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "AddressesVC") as! AddressesVC
        
        addressVC.mainViewModel.dicQuotationDetails = self.mainModelView.dicQuotationDetails
        addressVC.mainViewModel.dicAcceptedData = self.mainModelView.dicAcceptedData
        addressVC.mainViewModel.enumAddressCheck = .acceptedQuotation
        
        self.navigationController?.pushViewController(addressVC, animated: true)
    }
    
    @IBAction func btnRejectAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
}

//MARK: observer for table height
extension InvoiceVC {
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            
            self.mainView.tblDescription.layoutIfNeeded()
            self.mainView.tblDescription.layoutSubviews()
            
            if self.mainView.tblDescription.contentSize.height == 0 {
                self.mainView.tblDescriptionHeightConstrain.constant = 20
            }
            else {
                self.mainView.tblDescriptionHeightConstrain.constant = self.mainView.tblDescription.contentSize.height
            }
        }
    }
}
