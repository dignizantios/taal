//
//  PendingModel.swift
//  Taal
//
//  Created by Vishal on 27/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class PendingModelView {
    
    //MARK: Variables
    fileprivate weak var theController:PendingVC!
    var arrayData : [JSON] = []
    
    //MARK: Initializer
    init(theController: PendingVC) {
        self.theController = theController
    }
    
}

