//
//  PendingVC_UITableView.swift
//  Taal
//
//  Created by Vishal on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

//MARK: Tableview Delegate
extension PendingVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PendingTableCell", for: indexPath) as! PendingTableCell
        
        cell.vwQuotations.isHidden = false
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let detailVC = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "InvoiceVC") as! InvoiceVC
        detailVC.hidesBottomBarWhenPushed = true
        detailVC.mainModelView.isOpenRequestDetails = true
        self.navigationController?.pushViewController(detailVC, animated: false)
    }
    
}
