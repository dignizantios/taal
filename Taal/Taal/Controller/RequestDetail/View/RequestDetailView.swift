//
//  RequestDetailView.swift
//  Taal
//
//  Created by Vishal on 27/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

class RequestDetailView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet var lblRequest: UILabel!
    @IBOutlet weak var lblRequestValue: UILabel!
    @IBOutlet var lblCategoryTitle: UILabel!
    @IBOutlet var lblCategoryValue: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var tblDescription: UITableView!
    @IBOutlet weak var tblDescriptionHeightConstraint: NSLayoutConstraint!
    @IBOutlet var lblStatusTitle: UILabel!
    @IBOutlet var lblStatusValue: UILabel!
    @IBOutlet var lblQuatation: UILabel!
    @IBOutlet var btnQuotation: UIButton!
    @IBOutlet var btnBadge: CustomButton!
    
    @IBOutlet var lblQuotationsTitle: UILabel!
    @IBOutlet var requestCollection: UICollectionView!
    @IBOutlet weak var collectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var vwBottomError: UIView!
    @IBOutlet weak var btnEdit: CustomButton!
    @IBOutlet weak var btndelete: CustomButton!
    @IBOutlet weak var btnCancel: CustomButton!
    
    
    //MARK: SetUP
    func setUpUI(theDelegate: RequestDetailVC) {
        registerXIB()
        self.tblDescription.tableHeaderView = nil
        self.tblDescription.tableFooterView = nil
        vwMain.isHidden = true
        [lblRequest, lblRequestValue, lblCategoryTitle, lblDescription, lblStatusTitle].forEach { (lbl) in
            
            lbl?.font = themeFont(size: 18, fontname: .semiCn)
            lbl?.textColor = UIColor.appThemeBlueColor
        }
        
        [lblCategoryValue, lblStatusValue, lblQuatation].forEach { (lbl) in
            lbl?.font = themeFont(size: 15, fontname: .semiCn)
            lbl?.textColor = UIColor.appThemeBlueColor
        }
        
        [lblQuotationsTitle].forEach { (lbl) in
            
            lbl?.font = themeFont(size: 24, fontname: .semiCn)
            lbl?.textColor = UIColor.appThemeBlueColor
        }
        
        [btnEdit, btnCancel, btndelete].forEach { (btn) in
            btn?.setupThemeButtonUI()
            btn?.backgroundColor = UIColor.appThemeLightOrangeColor
        }
        
        self.lblRequest.text = "Request_key".localized
        self.lblCategoryTitle.text = "Categoty_key".localized
        self.lblDescription.text = "Description_key".localized
        self.lblStatusTitle.text = "Status_key".localized
        self.lblQuatation.text = "Quotations_key".localized
        self.lblQuotationsTitle.text = "Quotations_key".localized
        
        self.btnCancel.setTitle("Cancel_key".localized, for: .normal)
        self.btnEdit.setTitle("EDIT_key".localized, for: .normal)
        self.btndelete.setTitle("Delete_key".localized, for: .normal)
    }
    
    func registerXIB() {
        self.tblDescription.register(UINib(nibName: "DescriptionTableCell", bundle: nil), forCellReuseIdentifier: "DescriptionTableCell")
        self.requestCollection.register(UINib(nibName: "RequestDetailCollectionCell", bundle: nil), forCellWithReuseIdentifier: "RequestDetailCollectionCell")
    }
    
    func setUpData(data: CategoryDataModel) {
        
        tblDescription.reloadData()
        vwMain.isHidden = false
        self.lblRequestValue.text = "\(data.caseDetails!.caseId!)"
        self.lblCategoryValue.text = data.caseDetails?.category?.name
        self.lblStatusValue.text = data.caseDetails?.status
        self.requestCollection.reloadData()
        self.btnQuotation.setTitle("\(data.caseDetails?.numQuotations ?? 0)", for: .normal)
        self.btnBadge.isHidden = true
        
        if data.quotationDetails?.count == 0 {
            self.requestCollection.isHidden = true
            self.collectionHeightConstraint.constant = 115
            self.vwBottomError.isHidden = false
        }
        else {
            self.requestCollection.isHidden = false
            self.vwBottomError.isHidden = true
        }
    }
}
