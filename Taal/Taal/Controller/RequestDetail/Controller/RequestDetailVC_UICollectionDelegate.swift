//
//  RequestDetailVC_UICollectionDelegate.swift
//  Taal
//
//  Created by Vishal on 27/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

extension RequestDetailVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("Count: ", self.mainModelView.caseDetailData?.quotationDetails?.count ?? 0)
        return self.mainModelView.caseDetailData?.quotationDetails?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RequestDetailCollectionCell", for: indexPath) as! RequestDetailCollectionCell
        
        let dict = self.mainModelView.caseDetailData?.quotationDetails?[indexPath.row]
        cell.setUpData(data: dict!)
        
        cell.btnAccepted.tag = indexPath.row
        cell.btnViewReview.tag = indexPath.row
        cell.btnViewReview.addTarget(self, action: #selector(btnViewReviewAction(_:)), for: .touchUpInside)
        cell.btnAccepted.addTarget(self, action: #selector(btnAcceptedAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if self.mainModelView.caseDetailData?.quotationDetails?.count == 1 {
            return CGSize(width: collectionView.bounds.size.width-75, height: collectionView.bounds.size.height)
        }
        
        return CGSize(width: collectionView.bounds.size.width-100, height: collectionView.bounds.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
       return CGSize(width: 25, height: collectionView.frame.height)
     }
    
    @objc func btnAcceptedAction(_ sender: UIButton) {
        
        let quotationData = self.mainModelView.caseDetailData?.quotationDetails?[sender.tag]
        let invoiceVC = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "InvoiceVC") as! InvoiceVC
        invoiceVC.mainModelView.enumNavigationTitle = .acceptedQuotation
        invoiceVC.mainModelView.dicAcceptedData = self.mainModelView.caseDetailData
        invoiceVC.mainModelView.dicQuotationDetails = quotationData
        self.navigationController?.pushViewController(invoiceVC, animated: true)        
        
        
//        let addressVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "AddressesVC") as! AddressesVC
//
//        let quotationData = self.mainModelView.caseDetailData?.quotationDetails?[sender.tag]
//        addressVC.mainViewModel.dicQuotationDetails = quotationData
//        addressVC.mainViewModel.dicAcceptedData = self.mainModelView.caseDetailData
//        addressVC.mainViewModel.enumAddressCheck = .acceptedQuotation
//        self.navigationController?.pushViewController(addressVC, animated: true)
    }
    
    @objc func btnViewReviewAction(_ sender: UIButton) {
        
        let vc = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "ShowReviewVC") as! ShowReviewVC
        
        let providerID = "\(self.mainModelView.caseDetailData?.quotationDetails?[sender.tag].provider?.providerId ?? 0)"
        vc.mainModelView.providerID = providerID
        
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle   = .coverVertical
        self.present(vc, animated: false, completion: nil)
    }
}
