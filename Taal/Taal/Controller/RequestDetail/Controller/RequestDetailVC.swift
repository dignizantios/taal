//
//  RequestDetailVC.swift
//  Taal
//
//  Created by Vishal on 27/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class RequestDetailVC: UIViewController {

    //MARK: Variables
    lazy var mainView: RequestDetailView = { [unowned self] in
        return self.view as! RequestDetailView
        }()
    
    lazy var mainModelView: RequestDetailViewModel = {
        return RequestDetailViewModel(theController: self)
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI(theDelegate: self)
        self.mainModelView.handlorBaseButtonHide()
        self.mainModelView.getCaseDetailAPI(caseID: "\(self.mainModelView.categoryData?.caseId ?? 0)") {
            print("Success data....")
            self.mainView.setUpData(data: self.mainModelView.caseDetailData!)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitleRightAndBack(strTitle: "Request_detail_key".localized, barColor: UIColor.appThemeBlueColor, showTitle: true, isLeftBackButton: true, isRightSkipButton: false)
        self.mainView.tblDescription.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.mainView.tblDescription.removeObserver(self, forKeyPath: "contentSize")
        super.viewWillDisappear(true)
    }
    
}

//MARK: Button action
extension RequestDetailVC {
    
    @IBAction func btnEditAction(_ sender: Any) {
        
        let categoryVC = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "AddCategoryVC") as! AddCategoryVC
        categoryVC.mainModelView.dictCategoryValue = self.mainModelView.caseDetailData
        categoryVC.mainModelView.enumAddCase = .editCase
        self.navigationController?.pushViewController(categoryVC, animated: true)
    }
    
    @IBAction func btnDeleteAction(_ sender: Any) {
        
        self.mainModelView.deleteCaseAPI(caseID: "\(self.mainModelView.caseDetailData?.caseDetails?.caseId ?? 0)") {
            print("Delete")
            self.mainModelView.handlorDeleteCase()
            self.goBack(animated: true)
        }
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        
        self.mainModelView.caseCancelledAPI(caseID: "\(self.mainModelView.caseDetailData?.caseDetails?.caseId ?? 0)") {
            print("Cancel case")
            self.mainModelView.handlorDeleteCase()
            self.goBack()
        }
    }
}

//MARK: observer for table height
extension RequestDetailVC {
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            
            self.mainView.tblDescription.layoutIfNeeded()
            self.mainView.tblDescription.layoutSubviews()
            
            if self.mainView.tblDescription.contentSize.height == 0 {
                self.mainView.tblDescriptionHeightConstraint.constant = 20
            }
            else {
                self.mainView.tblDescriptionHeightConstraint.constant = self.mainView.tblDescription.contentSize.height
            }
        }
    }
}
