//
//  RequestDetailCollectionCell.swift
//  Taal
//
//  Created by Vishal on 27/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import HCSStarRatingView

class RequestDetailCollectionCell: UICollectionViewCell {

    //MARK: Outlets
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var vwStarRating: HCSStarRatingView!
    @IBOutlet var btnViewReview: UIButton!
    
    @IBOutlet var txtVW: UITextView!
    @IBOutlet var lblOffredPrice: UILabel!
    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var btnAccepted: CustomButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setUpUI()
    }

    func setUpUI() {
        
        [lblTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .semiCn)
            lbl?.textColor = UIColor.lightGray
        }
        
        [lblOffredPrice, lblAmount].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .semiCn)
            lbl?.textColor = UIColor.black
        }
        
        [btnViewReview].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 12, fontname: .semiCn)
            btn?.setTitleColor(UIColor.appThemeBlueColor, for: .normal)
        }
        
        [btnAccepted].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 18, fontname: .regular)
            btn?.setupThemeButtonUI()
            btn?.backgroundColor = UIColor.appThemeLightOrangeColor
        }
        btnViewReview.setTitle("view_reviews".localized, for: .normal)
        txtVW.textColor = UIColor.black
        txtVW.font = themeFont(size: 15, fontname: .semiCn)
        lblAmount.font = themeFont(size: 18, fontname: .semiCn)
        self.lblOffredPrice.text = "Offred_Price_key".localized
//        self.btnAccepted.setTitle("Accepted_key".localized, for: .normal)
        self.btnAccepted.setTitle("More information".localized, for: .normal)
    }
    
    func setUpData(data: QuotationDetails) {
        self.lblTitle.text = data.provider?.providerName
        self.txtVW.text = data.extraNotes
        self.lblAmount.text = data.price!+"KD_key".localized
        self.vwStarRating.value = CGFloat(exactly: data.provider?.providerRating ?? 0) ?? 0
    }
}
