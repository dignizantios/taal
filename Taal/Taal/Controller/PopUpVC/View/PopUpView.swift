//
//  PopUpView.swift
//  Taal
//
//  Created by Vishal on 27/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class PopUpView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet var btnDismiss: UIButton!
    @IBOutlet var lblMrFitOnWay: UILabel!
    @IBOutlet var btnStartTracking: UIButton!
    @IBOutlet var btnConfirm: UIButton!
    
    //MARK: SetUpUI
    func setUpUI(theController: PopUpVC) {
        
        [lblMrFitOnWay].forEach { (lbl) in
            lbl?.textColor = .black
            lbl?.font = themeFont(size: 18, fontname: .regular)
        }
        
        [btnStartTracking, btnConfirm].forEach { (btn) in
            btn?.setupThemeButtonUI()
            btn?.isHidden = true
            btn?.backgroundColor = UIColor.appThemeLightOrangeColor
        }
        
        self.vwMain.clipsToBounds = true
        self.vwMain.layer.cornerRadius = 25
        self.vwMain.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]   // Top right corner, Top left corner respectively
    }
    
    func setUpData(data: JSON) {
        
  //      jsonDict["job_status"].stringValue == "start_job" || jsonDict["job_status"].stringValue == arrivedAtLocation
        self.lblMrFitOnWay.text = "\(data["fullname"].stringValue)"+" \(data["message"].stringValue)"
        if data["job_status"].stringValue == "start_job" {
            self.btnStartTracking.isHidden = false
        }
        else {
            self.btnConfirm.isHidden = false
        }
    }
}
