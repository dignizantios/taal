//
//  PopUpViewModel.swift
//  Taal
//
//  Created by Vishal on 27/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class PopUpViewModel {
    
    fileprivate weak var theController:PopUpVC!
    
    //MARK: Initializer
    init(theController: PopUpVC) {
        self.theController = theController
    }
    var dictPushData = JSON()
    var handlorCaseChange:()->Void = {}
    var handlorCompleteJob:()->Void = {}
    
    //MARK: API Setup
    func caseChangeStatusAPI(params: [String:Any], completionHandlor:@escaping()->Void) {
        
        let url = caseChangeStatusURL
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        
        print("URL: ", url)
        print("Header: ", header)
        print("PARAM: ", params)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
//        self.theController.showLoader()
        WebServices().MakePatchAPI(name: url, params: params, header: header) { (response, error, statusCode) in
//            self.theController.hideLoader()
            if statusCode == success {
                if let dict = response as? NSDictionary {
                    let jsonDict = JSON(dict)
                    print("jsonDict: ", jsonDict)
                    self.dictPushData = jsonDict
                }
                else if let array = response as? NSArray {
                    let jsonArray = JSON(array)
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                completionHandlor()
            }
            else if error != nil {
                print("Error: ", error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode ?? 0)
            }
        }
    }
}
