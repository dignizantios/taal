//
//  PopUpVC.swift
//  Taal
//
//  Created by Vishal on 27/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class PopUpVC: UIViewController {

    //MARK: Variables
    lazy var mainView: PopUpView = { [unowned self] in
        return self.view as! PopUpView
        }()
    
    lazy var mainModelView: PopUpViewModel = {
        return PopUpViewModel(theController: self)
    }()
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getUserData()
        self.mainView.setUpUI(theController: self)
        self.mainView.setUpData(data: self.mainModelView.dictPushData)
    }
    
    //MARK: Button Action
    @IBAction func btnDismisAction(_ sender: Any) {
        
        self.dismissScreen(animated: true)
    }
    
    @IBAction func btnStartTrackingAction(_ sender: Any) {
        
//        if self.mainModelView.dictPushData[""].stringValue == "" {
//            self.mainModelView.caseChangeStatusAPI(params: [:]) {
//
//            }
//        }
        
        
        self.mainModelView.handlorCaseChange()
        self.dismissScreen(animated: false)
    }
    
    @IBAction func btnConfirmAction(_ sender: Any) {
        
        let param = ["case_id":dictNotificationData["record_id"].stringValue,
                     "status":7] as [String : Any]
        
        self.mainModelView.caseChangeStatusAPI(params: param) {
            self.mainModelView.handlorCompleteJob()
            self.dismissScreen(animated: false)
        }
    }
}
