//
//  ChangePasswordViewModel.swift
//  Taal
//
//  Created by Abhay on 29/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON


class ChangePasswordViewModel {
    
    fileprivate weak var theController : ChangePasswordVC!
    var countryCode = getUserData()?.user?.countryCode
    var loginDataDict = JSON()
    
    //MARK: Initializer
    init(theController: ChangePasswordVC) {
        self.theController = theController
    }
    
    
    //MARK:- checkIsMobileEmailExist
    func checkIsMobileEmailExist(param:[String:Any], completionHandlor:@escaping()->Void) {
        let url = userMobileEmailExistURL
        
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        theController.showLoader()
        WebServices().MakePostAPI(name: url, params: param, header: header) { (result, error, statusCode) in
            
            self.theController.hideLoader()
            if let data = result {
                let jsonData = JSON(data)
                if jsonData["success"].intValue == 1 {
                    makeToast(strMessage: jsonData["message"].stringValue)
                    return
                }
                else {
                    completionHandlor()
                    
                }
            }
            else if error != nil {
                makeToast(strMessage: error?.localized ?? "")
            }
            print("Error:-\(error ?? "")")
            print("StatusCode:-\(statusCode ?? 500)")
        }
    }
    
}
