//
//  ChangePasswordVC.swift
//  Taal
//
//  Created by Abhay on 29/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import SwiftyJSON
import Firebase

class ChangePasswordVC: UIViewController {

    //MARK: Variables
    lazy var mainView: ChangePasswordView = { [unowned self] in
        return self.view as! ChangePasswordView
        }()
    
    lazy var mainViewModel: ChangePasswordViewModel = {
        return ChangePasswordViewModel(theController: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.mainView.setUpUI(theController: self)
//        self.setUpNavigationBarWithTitleRightAndBack(strTitle: "Change_Mobile_key".localized, barColor: .appThemeBlueColor, showTitle: true, isLeftBackButton: true, isRightSkipButton: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpNavigationBarWithTitleRightAndBack(strTitle: "Change_Mobile_key".localized, barColor: .appThemeBlueColor, showTitle: true, isLeftBackButton: true, isRightSkipButton: false)
    }
    
    @IBAction func btnCountrySelectionAction(_ sender: Any) {
        
        let obj = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "CountryCodeVC") as! CountryCodeVC
        obj.modalPresentationStyle = .overFullScreen
        obj.mainView.delegate = self
        self.present(obj, animated: false, completion: nil)
    }
    
    @IBAction func btnUpdateAction(_ sender: Any) {
        
        if (self.mainView.txtMobileNumber.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please_enter_phone_number_key".localized)
        }
        else {
            
            let dict = ["mobile" : self.mainView.txtMobileNumber.text ?? "",
                        "country_code" : self.mainViewModel.countryCode ?? ""] as [String : Any]
            print("Param:- \(dict)")
            self.mainViewModel.loginDataDict = JSON(dict)
            self.mainViewModel.checkIsMobileEmailExist(param: dict) {
                self.setupMobileNumberOTP()
            }
        }
    }
}

extension ChangePasswordVC {
    
    func setupMobileNumberOTP() {
        
        let phoneNumber = "\(self.mainViewModel.countryCode ?? "")"+"\(self.mainView.txtMobileNumber.text!)"
        print("phoneNumber:-\(phoneNumber)")
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationId, error) in
            
            if error == nil {
                print("verificationId:-",verificationId!)
                guard let verify = verificationId else { return }
                userDefault.set(verify, forKey: "verifyID")
                
                let objc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "VerifyPhoneVC") as! VerifyPhoneVC
                objc.mainModelView.signupDataDict["mobile"].stringValue = self.mainView.txtMobileNumber.text ?? ""
                objc.mainModelView.verifyId = verify
                objc.mainModelView.selectController = .update
                objc.mainModelView.signupDataDict = self.mainViewModel.loginDataDict
                self.navigationController?.pushViewController(objc, animated: false)
            }
            else {
                print("Error:-\(error?.localizedDescription ?? "")")
            }
        }
    }
    
}

// MARK:- CountryCode Delegate
extension ChangePasswordVC : CountryCodeDelegate {
    
    func CountryCodeDidFinish(data: JSON) {
        
        print("data:\(data)")
        self.mainViewModel.countryCode = data["dial_code"].stringValue
        self.mainView.lblCountryCode.text = data["dial_code"].stringValue
//        self.theCurrentView.btnSelectCountryCodeOutlet.setTitle(data["dial_code"].stringValue, for: .normal)
    }
}
