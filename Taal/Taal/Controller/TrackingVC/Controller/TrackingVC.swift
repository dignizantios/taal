//
//  TrackingVC.swift
//  Taal
//
//  Created by Vishal on 02/12/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON
import FirebaseDatabase
import Polyline

class TrackingVC: UIViewController {
    
    //MARK: Variables
    lazy var mainView: TrackingView = {[unowned self] in
        return self.view as! TrackingView
        }()
    
    lazy var mainViewModel: TrackingViewModel = {
        return TrackingViewModel(theController: self)
    }()
    
    var marker = GMSMarker()
    var ref: DatabaseReference!
    var strConvesationID = String()
    var userLocation = CLLocationCoordinate2D()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*appDelegate.locationDelegate = self
         appDelegate.locationManager.startUpdatingLocation()*/
        self.setUpNavigationBarWithTitleRightAndBack(strTitle: "Tracking_key".localized, barColor: UIColor.appThemeBlueColor, showTitle: true, isLeftBackButton: true, isRightSkipButton: false)
        
        ref = Database.database().reference()
        updatePolyline()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        //        appDelegate.locationManager.stopUpdatingLocation()
    }
    
    /* func getHeadingForDirection(fromCoordinate fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Float {
     
     let fLat: Float = Float((fromLoc.latitude).degreesToRadians)
     let fLng: Float = Float((fromLoc.longitude).degreesToRadians)
     let tLat: Float = Float((toLoc.latitude).degreesToRadians)
     let tLng: Float = Float((toLoc.longitude).degreesToRadians)
     let degree: Float = (atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng))).radiansToDegrees
     if degree >= 0 {
     return degree
     }
     else {
     return 360 + degree
     }
     }*/
    
    func updatePolyline(){
        
        ref.child("tracking").child("\(strConvesationID)").observe(.value) { (snapshot) in
            
            let dict = JSON(snapshot.value)
            print("Polyline \(dict["polyline"].stringValue)")
            self.drawPath(strPolyLine: dict["polyline"].stringValue)
        }
    }
}


extension TrackingVC  {
    
    func setUpPinOnMap(lat:Double,long:Double,type:Int)  {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        if type == 1 {
            marker.icon = UIImage.init(named: "ic_map_pin_tracking")
        }
        else if type == 2 {
            let cameraCoord = CLLocationCoordinate2D(latitude: lat, longitude: long)
            //                self.mainView.googleView.camera = GMSCameraPosition.camera(withTarget: cameraCoord, zoom: 18)
            let updateCamera = GMSCameraUpdate.setTarget(cameraCoord, zoom: Float(15.0))
            self.mainView.googleView.animate(with: updateCamera)
            marker.icon = UIImage.init(named: "ic_map_pin")
        }
        else if type == 3{
            marker.icon = UIImage.init(named: "ic_map_pin")
        }
        
        marker.map = mainView.googleView
    }
    
    func drawPath(strPolyLine:String) {
        self.mainView.googleView.clear()
        let polyline = Polyline(encodedPolyline:strPolyLine)
        let decodedCoordinates: [CLLocationCoordinate2D] = polyline.coordinates!
        
        let path = GMSMutablePath()
        for i in 0..<decodedCoordinates.count {
            let dictFirst = decodedCoordinates[i]
            path.add(CLLocationCoordinate2D(latitude: dictFirst.latitude, longitude: dictFirst.longitude))
            if i == 0 {
                self.setUpPinOnMap(lat: Double(dictFirst.latitude), long: Double(dictFirst.longitude), type: 1)
            } else if i == ((decodedCoordinates.count) - 1) {
                self.setUpPinOnMap(lat: Double(dictFirst.latitude), long: Double(dictFirst.longitude), type: 2)
                /*if self.isRideStarted == false
                 {
                 let target = CLLocationCoordinate2D(latitude: dictFirst.latitude, longitude: dictFirst.longitude)
                 self.mainView.vwGoogleMaps.camera = GMSCameraPosition.camera(withTarget: target, zoom: 18)
                 }*/
            }
        }
//        setUpPinOnMap(lat: userLocation.latitude, long: userLocation.longitude, type: 3)
        
        
        let userLatitude = self.mainViewModel.dictCategoryData?.userDetails?.latitudeStr.toDouble()
        let userLongitude = self.mainViewModel.dictCategoryData?.userDetails?.longitudeStr.toDouble()
        print("USER Lat: \(userLatitude ?? 0.0), UserLongitude: \(userLongitude ?? 0.0)")
        
        setUpPinOnMap(lat: userLatitude ?? 0.0, long: userLongitude ?? 0.0, type: 3)
        let rectangle = GMSPolyline(path: path)
        rectangle.strokeWidth = 2
        rectangle.strokeColor = .appThemeRedColor
        rectangle.map = self.mainView.googleView
    }
    
    func fitAllMarkers(_path: GMSPath) {
        var bounds = GMSCoordinateBounds()
        for index in 1..._path.count() {
            bounds = bounds.includingCoordinate(_path.coordinate(at: index))
        }
        mainView.googleView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 20))
    }
}
    /* func getUpdateLocation(_ myLocation: CLLocationCoordinate2D) {
     
     //        print("Latitude:- \(myLocation.latitude)")
     //        print("Longitute:- \(myLocation.longitude)")
     //
     //        self.mainView.googleView.camera = GMSCameraPosition.camera(withLatitude: myLocation.latitude, longitude: myLocation.longitude, zoom: 16.0)
     //
     //        self.marker.map = nil
     //        marker = GMSMarker()
     //        marker = GMSMarker(position: CLLocationCoordinate2D(latitude: myLocation.latitude, longitude: myLocation.longitude))
     //
     //        let markerImg = UIImage(named: "vaspa")!.withRenderingMode(.alwaysTemplate)
     //        let markerVw = UIImageView(image: markerImg)
     //        markerVw.tintColor = UIColor.black
     //        marker.iconView = markerVw
     //
     //        marker.title = "Lokaci Pvt. Ltd."
     //        marker.snippet = "Sec 132 Noida India"
     //        marker.map = self.mainView.googleView
     
     var oldCoodinate: CLLocationCoordinate2D? = CLLocationCoordinate2DMake(self.mainViewModel.oldLocation?.latitude ?? 0.0, self.mainViewModel.oldLocation?.longitude ?? 0.0)
     var newCoodinate: CLLocationCoordinate2D? = CLLocationCoordinate2DMake(myLocation.latitude ?? 0.0,myLocation.longitude ?? 0.0)
     marker.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
     
     if self.mainViewModel.oldLocation != nil {
     
     self.mainView.googleView.camera = GMSCameraPosition.camera(withLatitude: myLocation.latitude, longitude: myLocation.longitude, zoom: 16.0)
     
     self.marker.map = nil
     marker = GMSMarker()
     marker = GMSMarker(position: CLLocationCoordinate2D(latitude: myLocation.latitude, longitude: myLocation.longitude))
     
     let markerImg = UIImage(named: "car")!.withRenderingMode(.alwaysTemplate)
     let markerVw = UIImageView(image: markerImg)
     markerVw.tintColor = UIColor.black
     marker.iconView = markerVw
     
     marker.rotation = CLLocationDegrees(getHeadingForDirection(fromCoordinate: self.mainViewModel.oldLocation!, toCoordinate: myLocation))
     //found bearing value by calculation when marker add
     marker.position = myLocation
     //this can be old position to make car movement to new position
     marker.map = self.mainView.googleView
     //marker movement animation
     CATransaction.begin()
     CATransaction.setValue(Int(2.0), forKey: kCATransactionAnimationDuration)
     CATransaction.setCompletionBlock({() -> Void in
     self.marker.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
     //            marker.rotation = CDouble(data.value(forKey: "bearing"))
     //New bearing value from backend after car movement is done
     })
     marker.position = myLocation
     //this can be new position after car moved from old position to new position with animation
     marker.map = self.mainView.googleView
     marker.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
     marker.rotation = CLLocationDegrees(getHeadingForDirection(fromCoordinate: self.mainViewModel.oldLocation!, toCoordinate: myLocation))
     //found bearing value by calculation
     CATransaction.commit()
     }
     self.mainViewModel.oldLocation = myLocation
     }*/


/*-extension Int {
 var degreesToRadians: Double { return Double(self) * .pi / 180 }
 }
 extension FloatingPoint {
 var degreesToRadians: Self { return self * .pi / 180 }
 var radiansToDegrees: Self { return self * 180 / .pi }
 }*/

//MARK: SetUpMap
extension TrackingVC {
    /*
     func setUpMap() {
     
     let sessionManager = SessionManager()
     let start = CLLocationCoordinate2D(latitude: 21.228125, longitude: 72.833771)
     let end = CLLocationCoordinate2D(latitude: 21.1959, longitude: 72.7933)
     
     sessionManager.requestDirections(from: start, to: end, completionHandler: { (path, error) in
     
     if let error = error {
     print("Something went wrong, abort drawing!\nError: \(error)")
     }
     else {
     // Create a GMSPolyline object from the GMSPath
     let polyline = GMSPolyline(path: path!)
     
     // Add the GMSPolyline object to the mapView
     polyline.map = self.mainView.googleView
     polyline.strokeColor = UIColor.red
     polyline.strokeWidth = 2
     
     // Move the camera to the polyline
     let bounds = GMSCoordinateBounds(path: path!)
     let cameraUpdate = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 40, left: 15, bottom: 10, right: 15))
     self.mainView.googleView.animate(with: cameraUpdate)
     
     }
     })
     }*/
}

/*
 class SessionManager {
 
 let GOOGLE_DIRECTIONS_API_KEY = key_google_place_picker
 
 func requestDirections(from start: CLLocationCoordinate2D, to end: CLLocationCoordinate2D, completionHandler: @escaping ((_ response: GMSPath?, _ error: Error?) -> Void)) {
 guard let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(start.latitude),\(start.longitude)&destination=\(end.latitude),\(end.longitude)&key=\(GOOGLE_DIRECTIONS_API_KEY)") else {
 let error = NSError(domain: "LocalDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to create object URL"])
 print("Error: \(error)")
 completionHandler(nil, error)
 return
 }
 
 // Set up the session
 let config = URLSessionConfiguration.default
 let session = URLSession(configuration: config)
 
 let task = session.dataTask(with: url) { (data, response, error) in
 // Check if there is an error.
 guard error == nil else {
 DispatchQueue.main.async {
 print("Google Directions Request Error: \((error!)).")
 completionHandler(nil, error)
 }
 return
 }
 
 // Make sure data was received.
 guard let data = data else {
 DispatchQueue.main.async {
 let error = NSError(domain: "GoogleDirectionsRequest", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to receive data"])
 print("Error: \(error).")
 completionHandler(nil, error)
 }
 return
 }
 
 do {
 // Convert data to dictionary.
 guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
 DispatchQueue.main.async {
 let error = NSError(domain: "GoogleDirectionsRequest", code: 2, userInfo: [NSLocalizedDescriptionKey: "Failed to convert JSON to Dictionary"])
 print("Error: \(error).")
 completionHandler(nil, error)
 }
 return
 }
 
 // Check if the the Google Direction API returned a status OK response.
 guard let status: String = json["status"] as? String, status == "OK" else {
 DispatchQueue.main.async {
 let error = NSError(domain: "GoogleDirectionsRequest", code: 3, userInfo: [NSLocalizedDescriptionKey: "Google Direction API did not return status OK"])
 print("Error: \(error).")
 completionHandler(nil, error)
 }
 return
 }
 
 print("Google Direction API response:\n\(json)")
 
 // We only need the 'points' key of the json dictionary that resides within.
 if let routes: [Any] = json["routes"] as? [Any], routes.count > 0, let routes0: [String: Any] = routes[0] as? [String: Any], let overviewPolyline: [String: Any] = routes0["overview_polyline"] as? [String: Any], let points: String = overviewPolyline["points"] as? String {
 // We need the get the first object of the routes array (route0), then route0's overview_polyline and finally overview_polyline's points object.
 
 if let path: GMSPath = GMSPath(fromEncodedPath: points) {
 DispatchQueue.main.async {
 completionHandler(path, nil)
 }
 return
 } else {
 DispatchQueue.main.async {
 let error = NSError(domain: "GoogleDirections", code: 5, userInfo: [NSLocalizedDescriptionKey: "Failed to create GMSPath from encoded points string."])
 completionHandler(nil, error)
 }
 return
 }
 
 } else {
 DispatchQueue.main.async {
 let error = NSError(domain: "GoogleDirections", code: 4, userInfo: [NSLocalizedDescriptionKey: "Failed to parse overview polyline's points"])
 completionHandler(nil, error)
 }
 return
 }
 
 
 } catch let error as NSError  {
 DispatchQueue.main.async {
 completionHandler(nil, error)
 }
 return
 }
 
 }
 
 task.resume()
 }
 }
 */
