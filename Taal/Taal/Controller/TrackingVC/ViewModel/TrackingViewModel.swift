//
//  TrackingViewModel.swift
//  Taal
//
//  Created by Vishal on 02/12/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import SwiftyJSON

class TrackingViewModel {
    
    //MARK: Variables
    fileprivate weak var theController: TrackingVC!
    var locationDelegate: locationUpdateDelegate?
    var oldLocation: CLLocationCoordinate2D?
    var dictCategoryData = CategoryDataModel(JSON: JSON().dictionaryObject!)
    
    
    //MARK: Initializer
    init(theController: TrackingVC) {
        self.theController = theController
    }
    
    
}
