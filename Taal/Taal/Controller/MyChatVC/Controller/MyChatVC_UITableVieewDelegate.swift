//
//  MyChatVC_UITableVieewDelegate.swift
//  Taal
//
//  Created by Abhay on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

extension MyChatVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.mainModelView.arrayChatList.count == 0 {
            setTableView("No_record_found_key".localized, tableView: self.mainView.tblMyChat)
            return 0
        }
        
        tableView.backgroundView = nil
        return self.mainModelView.arrayChatList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyChatCell") as! MyChatCell
        
        let dict = self.mainModelView.arrayChatList[indexPath.row]
        cell.lblName.text = dict.fullname
        cell.lblTime.text = dict.chatDict?.time ?? ""
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 90//UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ChatDetailsVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "ChatDetailsVC") as! ChatDetailsVC
        let dict = self.mainModelView.arrayChatList[indexPath.row]
        ChatDetailsVC.mainModelView.userName = dict.fullname
        ChatDetailsVC.mainModelView.conversatinID = dict.conversationId
        
        self.navigationController?.pushViewController(ChatDetailsVC, animated: true)
    }
}
