//
//  MyChatVC.swift
//  Taal
//
//  Created by Abhay on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class MyChatVC: UIViewController {
    //MARK:- Variables
    lazy var mainView: MyChatView = { [unowned self] in
        return self.view as! MyChatView
        }()
    
    lazy var mainModelView: MyChatViewModel = {
        return MyChatViewModel(theController: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        mainView.setupUI(theDelegate: self)
        self.setUpPullToRefresh()
        self.showLoader()
        self.mainModelView.userChatListAPI {
            self.mainView.tblMyChat.reloadData()
        }
        
        isNotificationChatVC()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setUpNavigationBarWithTitleRightAndBack(strTitle: "My_Chats_key".localized, barColor: UIColor.appThemeBlueColor, showTitle: true, isLeftBackButton: true, isRightSkipButton: false)
    }

/*
     let ChatDetailsVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "ChatDetailsVC") as! ChatDetailsVC
     let dict = self.mainModelView.arrayChatList[indexPath.row]
     ChatDetailsVC.mainModelView.userName = dict.fullname
     ChatDetailsVC.mainModelView.conversatinID = dict.conversationId
     
     self.navigationController?.pushViewController(ChatDetailsVC, animated: true)
     */
    
    func isNotificationChatVC() {
        
        if isNotificationChat {
            
            let ChatDetailsVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "ChatDetailsVC") as! ChatDetailsVC
            
            isNotificationChat = false
//            print("Notification Data: ", dictNotificationData)
            ChatDetailsVC.mainModelView.userName = dictNotificationData["fullname"].stringValue
            ChatDetailsVC.mainModelView.conversatinID = dictNotificationData["conversation_id"].stringValue
            self.navigationController?.pushViewController(ChatDetailsVC, animated: false)
        }
    }
}

//MARK: SetUpUI
extension MyChatVC {
    
    func setUpPullToRefresh() {
        self.mainModelView.refreshController = UIRefreshControl()
        self.mainModelView.refreshController.tintColor = UIColor.appThemeBlueColor
        self.mainModelView.refreshController.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        self.mainView.tblMyChat.addSubview(self.mainModelView.refreshController)
    }
    
    @objc func pullToRefresh() {
        self.mainModelView.refreshController.endRefreshing()
        self.showLoader()
        self.mainModelView.userChatListAPI {
            self.mainView.tblMyChat.reloadData()
        }
    }
}
