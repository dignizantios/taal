//
//  RequestHistryVC_UITableViewDelegate.swift
//  Taal
//
//  Created by Vishal on 28/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

extension RequestHistryVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.mainView.tblRequestHistry {
            if self.mainModelView.arrayCaseData.count == 0 {
                setTableView("No_record_found_key".localized, tableView: self.mainView.tblRequestHistry)
                return 0
            }
            tableView.backgroundView = nil
            return self.mainModelView.arrayCaseData.count
        }
        
        let filter = self.mainModelView.arrayCaseData[tableView.tag].fields.filter { (dict) -> Bool in
            let voice = dict.type?.lowercased() != "Voice".lowercased()
            let img = dict.type != "image"
            return voice && img
        }

        return filter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.mainView.tblRequestHistry {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PendingTableCell", for: indexPath) as! PendingTableCell
            
            let data = self.mainModelView.arrayCaseData[indexPath.row]
            cell.setUpData(data: data)
            
            cell.btnWriteReview.tag = indexPath.row
            cell.btnWriteReview.addTarget(self, action: #selector(btnWriteReviewAction(_:)), for: .touchUpInside)
            
            cell.vwQuotations.isHidden = true
            cell.vwWriteReview.isHidden = false
            
            if data.statusId == 2 {
                cell.vwQuotations.isHidden = true
                cell.vwWriteReview.isHidden = true
            }
            
            cell.tblDescription.tag = indexPath.row
            cell.tblDescription.delegate = self
            cell.tblDescription.dataSource = self
            cell.tblDescription.reloadData()
            cell.tblDescription.layoutIfNeeded()
            cell.tblDescription.setNeedsLayout()
            cell.tblDescription.layoutSubviews()
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionTableCell", for: indexPath) as! DescriptionTableCell
        
        let filter = self.mainModelView.arrayCaseData[tableView.tag].fields.filter { (dict) -> Bool in
            let voice = dict.type?.lowercased() != "Voice".lowercased()
            let img = dict.type != "image"
            return voice && img
        }
        
        cell.backgroundColor = .clear
        cell.vwMain.backgroundColor = .clear
        
        let dict = filter[indexPath.row]
        cell.lblDescriptionTitle.text = "\(dict.label!)"+": "
        cell.lblDescriptionValue.text = dict.value?.first ?? ""
        
        return cell
    }
    
    @objc func btnWriteReviewAction(_ sender:UIButton) {
        
        let reviewVC = AppStoryboard.Profile.instance.instantiateViewController(withIdentifier: "ReviewVC") as! ReviewVC
        let data = self.mainModelView.arrayCaseData[sender.tag]
        reviewVC.mainModelView.dictSelectCategoryData = data
        self.navigationController?.pushViewController(reviewVC, animated: true)
    }
}
