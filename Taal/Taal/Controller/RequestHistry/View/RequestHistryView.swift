//
//  RequestHistryView.swift
//  Taal
//
//  Created by Vishal on 28/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

class RequestHistryView: UIView {
    	
    //MARK: Outlets
    @IBOutlet weak var btnSuccess: UIButton!
    @IBOutlet weak var vwBottomSuccess: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var vwBottomCancel: UIView!
    
    @IBOutlet var tblRequestHistry: UITableView!
    
    //MARK: SetUp
    func setUpUI(theController: RequestHistryVC) {
        
        vwBottomSuccessShow()
        registerXib()
        
        [btnSuccess, btnCancel].forEach { (btn) in
            btn?.setTitleColor(UIColor.white, for: .normal)
            btn?.setupThemeButtonUI()
            btn?.titleLabel?.font = themeFont(size: 17, fontname: .boldSemi)
            btn?.layer.cornerRadius = 0
        }
        btnCancel.setTitle("Cancel_key".localized, for: .normal)
        btnSuccess.setTitle("Success".localized, for: .normal)
        
    }
    
    func registerXib() {
        
        tblRequestHistry.register(UINib(nibName: "PendingTableCell", bundle: nil), forCellReuseIdentifier: "PendingTableCell")
    }
    
    func vwBottomSuccessShow() {
        vwBottomSuccess.isHidden = false
        vwBottomCancel.isHidden = true
    }
    
    func vwBottomCancelShow() {
        vwBottomSuccess.isHidden = true
        vwBottomCancel.isHidden = false
    }
    
}
