//
//  RequestHistryViewModel.swift
//  Taal
//
//  Created by Vishal on 28/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON


class RequestHistryViewModel {

    fileprivate weak var theController: RequestHistryVC!
    
    //MARK: Initializer
    init(theController: RequestHistryVC) {
        self.theController = theController
    }
    var refreshController: UIRefreshControl!
    var arrayCaseData : [CategoryDataModel] = []
    var caseID = "3"
    
    //MARK: GetHistoryData
    func getCasesHistoryAPI(statusID: String, completionHandlor:@escaping()->Void) {
        
        let url = getCaseHistoryURL+statusID
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        
        print("URL: ", url)
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        self.theController.showLoader()
        
        WebServices().MakeGetAPI(name: url, params: [:], header: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            self.refreshController.endRefreshing()
            self.arrayCaseData = []
            if statusCode == success {
                if let array = response as? NSArray {
                    let jsonArray = JSON(array).arrayValue
                    print("jsonArray: ", jsonArray)
                    for data in jsonArray {
                        let caseData = CategoryDataModel(JSON: data.dictionaryObject!)
                        self.arrayCaseData.append(caseData!)
                    }
                }
                else if let dict = response as? NSDictionary {
                    let jsonDict = JSON(dict).dictionaryValue
                    print("jsonDict: ", jsonDict)
                    
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                completionHandlor()
            }
            else if error != nil {
                print("Error: ", error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode ?? 0)
            }
        }
    }
}
